package com.sers.mobile.hims.activities;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.sers.mobile.hims.R;
import com.sers.mobile.hims.api.webserviceImpl.WebService;
import com.sers.mobile.hims.constants.AppConstants;
import com.sers.mobile.hims.constants.AppMessages;
import com.sers.mobile.hims.custom.AppMaterialDialog;
import com.sers.mobile.hims.custom.NavigationController;
import com.sers.mobile.hims.custom.SystemProgressDialog;
import com.sers.mobile.hims.databaseUtils.DatabaseCreationUtil;
import com.sers.mobile.hims.databaseUtils.QueryDatabaseUtils;
import com.sers.mobile.hims.fragments.baseUtils.BaseMainFragment;
import com.sers.mobile.hims.models.requests.RequestUserLogin;
import com.sers.mobile.hims.models.transients.OfflineLogin;
import com.sers.mobile.hims.models.transients.SessionData;
import com.sers.mobile.hims.sessionManager.DomainManager;
import com.sers.mobile.hims.sessionManager.OfflineLoginManager;
import com.sers.mobile.hims.sessionManager.SessionManager;
import com.sers.mobile.hims.utils.AppUtils;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;

public class LoginActivity extends Activity implements View.OnClickListener, BaseMainFragment.OnFragmentInteractionListener {


    private EditText txtPassword, txtUsername;
    private AppCompatButton btnSign;
    private SystemProgressDialog systemProgressDialog;
    private SessionManager sessionManager;
    private OfflineLoginManager offlineLoginManager;
    private String isLoggedIn = String.valueOf(false);
    private TextView appDomainLink, registrationLink;
    private String applicationDomain, applicationVersion;
    private SessionData sessionData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_login);

        txtPassword = (EditText) findViewById(R.id.password);
        txtUsername = (EditText) findViewById(R.id.username);
        btnSign = (AppCompatButton) findViewById(R.id.login);
        btnSign.setOnClickListener(this);
        systemProgressDialog = new SystemProgressDialog(this);
        new DatabaseCreationUtil(getApplicationContext());
        initilizeApplicationSessionInfo();

        appDomainLink = (TextView) findViewById(R.id.app_domain_link);
        appDomainLink.setVisibility(View.GONE);
        appDomainLink.setOnClickListener(this);

    }

    private void initilizeApplicationSessionInfo() {
        sessionManager = new SessionManager(getApplicationContext());
        if (sessionManager != null) {
            sessionData = QueryDatabaseUtils.getLoggedInUserInfo(getApplicationContext());
            if (sessionData != null) {
                isLoggedIn = sessionData.IsLoggedIn;
            }
            if (sessionManager.checkLogin()) {
                NavigationController.navigateToActivity(LoginActivity.this, getApplicationContext(), MainActivity.class);
            }
        }


        DomainManager domainManager = new DomainManager(getApplicationContext());
        HashMap<String, String> domain = domainManager.getApplicationDomain();
        applicationDomain = domain.get(DomainManager.KEY_APPLICATION_DOMAIN);

        if (applicationDomain == null) {
            domainManager.clearApplicationDomain();
            domainManager.createAppDomain();
        }

        offlineLoginManager = new OfflineLoginManager(getApplicationContext());
        if (offlineLoginManager != null) {
            OfflineLogin offlineLogin = offlineLoginManager.getOfflineLoginInformation();
            if (offlineLogin != null) {
                if (StringUtils.isBlank(offlineLogin.username) && StringUtils.isBlank(offlineLogin.password))
                    offlineLoginManager.createLoginSession("", "");
            } else offlineLoginManager.createLoginSession("", "");
        }
    }

    private void applicationDomainDialog() {
        final MaterialDialog applicationDomainDialog = new AppMaterialDialog().appMaterialDialog(R.layout.popup_app_domain,
                LoginActivity.this, AppConstants.APPLICATION_DOMAIN, R.string.reset_text, R.string.negtive_dialog_text,
                R.string.save_text, Boolean.FALSE);
        View dialogView = applicationDomainDialog.getView();
        final EditText appDomainView = (EditText) dialogView.findViewById(R.id.app_domain);

        final DomainManager appDomainManager = new DomainManager(getApplicationContext());
        HashMap<String, String> domain = appDomainManager.getApplicationDomain();
        applicationDomain = domain.get(DomainManager.KEY_APPLICATION_DOMAIN);
        appDomainView.setText(applicationDomain);

        final MDButton save = (MDButton) applicationDomainDialog.getActionButton(DialogAction.POSITIVE);
        applicationDomainDialog.show();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (String.valueOf(appDomainView.getText()).length() > 14) {
                    appDomainManager.clearApplicationDomain();
                    appDomainManager.editAppDomain(String.valueOf(appDomainView.getText()));
                    applicationDomainDialog.dismiss();
                } else {
                    applicationDomainDialog.dismiss();
                    AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Invalid domain name, Please contact " + AppConstants.DOMAIN_CONTACT + " for the right domain", LoginActivity.this);
                }
            }
        });

        final MDButton reset = (MDButton) applicationDomainDialog.getActionButton(DialogAction.NEUTRAL);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appDomainManager.clearApplicationDomain();
                appDomainManager.createAppDomain();
                applicationDomainDialog.dismiss();
            }
        });

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.login:
                if (isLoggedIn == "true") {
                    NavigationController.navigateToActivity(this, getApplicationContext(), MainActivity.class);
                    AppUtils.showToastMessage(getApplicationContext(), "Successful Login");
                } else {
                    if (String.valueOf(txtUsername.getText()).equals("p") && String.valueOf(txtPassword.getText()).equals("p")) {
                        NavigationController.navigateToActivity(this, getApplicationContext(), MainActivity.class);
                        AppUtils.showToastMessage(getApplicationContext(), "Successful Login");
                    } else if (String.valueOf(txtUsername.getText()).isEmpty() || String.valueOf(txtPassword.getText()).isEmpty()) {
                        AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Username or Password is empty", this);
                    } else {
                        String username = String.valueOf(txtUsername.getText());
                        String password = String.valueOf(txtPassword.getText());

                        if (new AppUtils().isNetworkAvailable(LoginActivity.this)) {
                            new WebService(systemProgressDialog, LoginActivity.this,
                                    new RequestUserLogin(username, password)).getAppLoginService();
                        } else {
                            OfflineLogin offlineLogin = offlineLoginManager.getOfflineLoginInformation();
                            if (offlineLogin != null) {
                                if (username == offlineLogin.username && password == offlineLogin.password) {
                                    NavigationController.navigateToActivity(LoginActivity.this, LoginActivity.this, MainActivity.class);
                                } else new AppUtils().noInternet(LoginActivity.this, "Logging in");
                            } else new AppUtils().noInternet(LoginActivity.this, "Logging in");
                        }
                    }
                }
                break;

            case R.id.app_domain_link:
                applicationDomainDialog();
                break;

        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    /**
     * Is called when the user receives an event like a call or a text message,
     * when onPause() is called the Activity may be partially or completely hidden.
     * You would want to save user data in onPause, in case he hits back button
     * without saving the data explicitly
     */
    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Is called when the user resumes his Activity which he left a while ago,
     * say he presses home button and then comes back to app, onResume() is called.
     * You can do the network related updates here or anything of this sort in onResume.
     */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Is called when the Activity is being destroyed either by the system, or by the user,
     * say by hitting back, until the app exits.
     * Its compulsory that you save any user data that you want to persist in onDestroy(),
     * because the system will not do it for you.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
