package com.sers.mobile.hims.fragments.baseUtils;


import android.app.Activity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.melnykov.fab.FloatingActionButton;
import com.sers.mobile.hims.R;
import com.sers.mobile.hims.custom.AppMaterialDialog;
import com.sers.mobile.hims.custom.SystemProgressDialog;
import com.sers.mobile.hims.pagination.CustomPaginator;
import com.sers.mobile.hims.utils.AppUtils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

public abstract class BaseReportFragment extends CustomPaginator
        implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, DatePickerDialog.OnDateSetListener {

    public RecyclerView recyclerView;
    public SwipeRefreshLayout swipeRefreshLayout;
    public SystemProgressDialog systemProgressDialog;
    public int VIEW_CHANGER = 0;
    public EditText searchStartDate, searchEndDate;
    public MDButton confirmButton;
    public LinearLayout searchLinearLayout, toggleLayoutView, swipeLinearLayout, otherReportFilters;
    public RelativeLayout formDisplayLayout;
    public EditText searchEditField;
    public ToggleButton toggleButton;
    public FloatingActionButton fab;
    public AppCompatButton toggleSearchButton;

    public BaseReportFragment(Activity activity) {
        super(activity);
    }

    public BaseReportFragment() {
        super();

    }


    public void loadContent() {
        this.getContent(false);
    }

    @Override
    public void onRefresh() {
        this.getContent(true);
    }

    public abstract void getContent(boolean isRefreshing);

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        if (VIEW_CHANGER == 1) {
            int monthday_start = monthOfYear + 1;
            if (searchStartDate != null)
                searchStartDate.setText(dayOfMonth + "/" + monthday_start + "/" + year);
        } else if (VIEW_CHANGER == 2) {
            int monthday_end = monthOfYear + 1;
            if (searchEndDate != null)
                searchEndDate.setText(dayOfMonth + "/" + monthday_end + "/" + year);
        }
    }


    public void initializeStartAndEndDatesPickers(final DatePickerDialog.OnDateSetListener onDateSetListener) {
        if (searchStartDate != null)
            searchStartDate.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == 0) {
                        VIEW_CHANGER = 1;
                        showDatePicker(onDateSetListener);
                    }
                    return false;
                }
            });

        if (searchEndDate != null)
            searchEndDate.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == 0) {
                        VIEW_CHANGER = 2;
                        showDatePicker(onDateSetListener);
                    }
                    return false;
                }
            });
    }

    public void showDatePicker(DatePickerDialog.OnDateSetListener onDateSetListener) {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                onDateSetListener,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getActivity().getFragmentManager(), "Select Date");
        dpd.setAccentColor(getResources().getColor(R.color.colorPrimary));
    }


    public void calendarDialogOpener(View view, DatePickerDialog.OnDateSetListener onDateSetListener, View.OnClickListener onClickListener, final String floatTransactionType) {
        final MaterialDialog materialDialog = new AppMaterialDialog().appMaterialDialog(R.layout.popup_calendar_filter,
                getActivity(), "Select Date Interval", 0, R.string.dialog_close,
                R.string.dialog_confirm, Boolean.FALSE);

        View dialog_view = materialDialog.getView();
        searchStartDate = (EditText) dialog_view.findViewById(R.id.searchStartDate);
        searchEndDate = (EditText) dialog_view.findViewById(R.id.searchEndDate);
        initializeStartAndEndDatesPickers(onDateSetListener);
        confirmButton = (MDButton) materialDialog.getActionButton(DialogAction.POSITIVE);
        materialDialog.show();

        if (new AppUtils().checkNetwork(getActivity().getApplicationContext(), getActivity())) {
            confirmButton.setVisibility(View.VISIBLE);
            confirmButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                }
            });
        }

    }


    public void searchRecords() {
        if (searchEditField != null) {
            searchEditField.addTextChangedListener(getSearchViewTextWatcher(searchEditField));
        } else {
            loadContent();
        }
    }

    public abstract void doSearch(String searchValue);

    public TextWatcher getSearchViewTextWatcher(final EditText searchEditField) {
        return new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

                if (filterLongEnough()) {
                    String searchValue = (searchEditField.getText() != null ? String.valueOf(searchEditField.getText()) : null);
                    doSearch(searchValue);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }

            private boolean filterLongEnough() {
                return searchEditField.getText() != null ? (String.valueOf(searchEditField.getText()) != null ?
                        (String.valueOf(searchEditField.getText()).trim().length() > 0 ? true : false) : false) : false;
            }
        };
    }


}
