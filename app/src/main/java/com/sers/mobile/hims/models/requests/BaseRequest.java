package com.sers.mobile.hims.models.requests;


import com.sers.mobile.hims.models.transients.SessionData;

public class BaseRequest {
    private String username;
    private String apiToken;

    public BaseRequest() {
    }

    public BaseRequest(SessionData sessionData) {
        this.username = sessionData.Username;
        this.apiToken = sessionData.ApiToken;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }
}
