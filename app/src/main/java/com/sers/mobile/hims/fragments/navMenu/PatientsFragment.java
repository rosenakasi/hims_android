package com.sers.mobile.hims.fragments.navMenu;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;

import com.sers.mobile.hims.R;
import com.sers.mobile.hims.adapters.recyclerAdapters.PatientCardAdapter;
import com.sers.mobile.hims.constants.AppConstants;
import com.sers.mobile.hims.constants.AppMessages;
import com.sers.mobile.hims.constants.RecordStatus;
import com.sers.mobile.hims.custom.SystemProgressDialog;
import com.sers.mobile.hims.databaseUtils.QueryDatabaseUtils;
import com.sers.mobile.hims.fragments.baseUtils.BaseReportFragment;
import com.sers.mobile.hims.models.database.Disease;
import com.sers.mobile.hims.models.database.Patient;
import com.sers.mobile.hims.models.transients.CheckBoxItem;
import com.sers.mobile.hims.models.transients.SessionData;
import com.sers.mobile.hims.utils.AppUtils;

import org.apache.commons.lang3.StringUtils;
import org.sers.kpa.datasource.datamanager.CustomPersistanceManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class PatientsFragment extends BaseReportFragment {

    private List<Patient> patients = new ArrayList<Patient>();
    private SessionData sessionData;
    private AppCompatButton formButton;
    private String selectedGender;
    private EditText patientIdField, patientNameField, patientPhoneNumberField, patientVillageNameField, patientMeterNumberField, patientMethodOfDiagnosisField, patientResultOfDiagnosisField;
    private RadioButton patientGenderMale, patientGenderFemale, patientGenderUnknown;
    private ArrayList<CheckBoxItem> selectedCheckBoxValue = new ArrayList<CheckBoxItem>();
    private LinearLayout leftLinearLayout, rightLinearLayout;

    public PatientsFragment() {
        super();
    }


    @SuppressLint("ValidFragment")
    public PatientsFragment(Activity activity, Toolbar toolbar, FragmentManager superFragmentManager) {
        super(activity);
        this.toolbar = toolbar;
        super.compactibleFragmentManager = superFragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_records_view, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.listRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplication().getApplicationContext()));
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.listSwipeView);
        swipeRefreshLayout.setOnRefreshListener(this);
        systemProgressDialog = new SystemProgressDialog(getActivity());
        sessionData = QueryDatabaseUtils.getLoggedInUserInfo(getActivity());

        searchLinearLayout = (LinearLayout) rootView.findViewById(R.id.searchLayoutView);
        searchLinearLayout.setVisibility(View.GONE);
        searchEditField = (EditText) rootView.findViewById(R.id.searchEditField);
        searchRecords();

        loadContent();

        toggleLayoutView = (LinearLayout) rootView.findViewById(R.id.toggleLayoutView);
        formDisplayLayout = (RelativeLayout) rootView.findViewById(R.id.formDisplayLayout);
        swipeLinearLayout = (LinearLayout) rootView.findViewById(R.id.swipeLinearLayout);
        swipeLinearLayout.setVisibility(View.VISIBLE);

        initialiseFilterForm(rootView);

        formButton = (AppCompatButton) rootView.findViewById(R.id.button);
        toggleLayoutView.setVisibility(View.VISIBLE);
        toggleButton = (ToggleButton) rootView.findViewById(R.id.toggleButton);
        toggleButton.setText("VIEW PATIENTS");
        toggleButton.setTextOff("VIEW PATIENTS");
        toggleButton.setTextOn("ADD PATIENT INFORMATION");
        toggleButton.setChecked(false);
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    System.out.println("Swipe is on ...");
                    formDisplayLayout.setVisibility(View.GONE);
                    searchLinearLayout.setVisibility(View.VISIBLE);
                    swipeLinearLayout.setVisibility(View.VISIBLE);
                } else {
                    System.out.println("Swipe is off ...");
                    swipeLinearLayout.setVisibility(View.GONE);
                    searchLinearLayout.setVisibility(View.GONE);
                    formDisplayLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        formButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidReportFilterDetails()) {
                    List<CheckBoxItem> checkedDiseases = selectedCheckBoxValue;
                    if (checkedDiseases != null) {
                        if (checkedDiseases.size() > 0) {

                            List<String> itemIds = new ArrayList<String>();

                            StringBuilder diseaseIds = new StringBuilder();
                            StringBuilder diseaseNames = new StringBuilder();
                            for (CheckBoxItem checkBoxItem : checkedDiseases) {
                                Disease disease = new QueryDatabaseUtils().getDiseaseByPrimaryKey(checkBoxItem.id);
                                if (disease != null) {
                                    diseaseIds.append(disease.id + ",");
                                    diseaseNames.append(disease.name + ",");
                                }
                            }


                            String name = String.valueOf(patientNameField.getText());
                            String patientId = String.valueOf(patientIdField.getText());
                            String dateOfBirth = String.valueOf(searchStartDate.getText());
                            String phoneNumber = String.valueOf(patientPhoneNumberField.getText());
                            String villageName = String.valueOf(patientVillageNameField.getText());
                            String methodOfDiagnosis = String.valueOf(patientMethodOfDiagnosisField.getText());
                            String resultOfDiagnosis = String.valueOf(patientResultOfDiagnosisField.getText());
                            String dateOfDiagnosis = String.valueOf(searchEndDate.getText());
                            String diseaseId = String.valueOf(diseaseIds);
                            String diseaseName = String.valueOf(diseaseNames);
                            String status = RecordStatus.UN_SYNCED.getStatus();
                            String recordId = AppUtils.generateUserId();
                            String nwscMeterNumber = String.valueOf(patientMeterNumberField.getText());
                            String date = AppUtils.getCurrentDate();

                            Patient patient = new Patient("", date, patientId, name, dateOfBirth, selectedGender, phoneNumber,
                                    nwscMeterNumber, diseaseId, diseaseName, methodOfDiagnosis, resultOfDiagnosis,
                                    dateOfDiagnosis, recordId, status, villageName);
                            patient.userId = sessionData.UserId;
                            try {
                                Patient savedPatient = new CustomPersistanceManager<Patient>(Patient.class).saveOrUpdateAndReturnModel(patient);
                                if (savedPatient != null) {
                                    List<Patient> patientList = new QueryDatabaseUtils().getPatients((sessionData != null ? sessionData.UserId : "n.a"));
                                    if (patientList != null) {
                                        if (patientList.size() > 0) {
                                            patients = patientList;
                                            recyclerView.setAdapter(new PatientCardAdapter(R.layout.card_row_patient_layout, getActivity(), systemProgressDialog, getActivity(),
                                                    patients, sessionData, toolbar, getFragmentManager(), searchLinearLayout,
                                                    toggleLayoutView, swipeLinearLayout, formDisplayLayout, toggleButton));
                                            formDisplayLayout.setVisibility(View.GONE);
                                            searchLinearLayout.setVisibility(View.VISIBLE);
                                            swipeLinearLayout.setVisibility(View.VISIBLE);

                                            patientNameField.setText("");
                                            patientIdField.setText("");
                                            patientPhoneNumberField.setText("");
                                            patientVillageNameField.setText("");
                                            patientMeterNumberField.setText("");
                                            patientMethodOfDiagnosisField.setText("");
                                            patientResultOfDiagnosisField.setText("");
                                            searchStartDate.setText("");
                                            searchEndDate.setText("");

                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else
                            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Please select at least one disease and try again", getActivity());
                    } else
                        AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Please select at least one disease and try again", getActivity());
                }
            }
        });

        return rootView;
    }

    @Override
    public boolean headerInformationIsValid() {
        return false;
    }

    @Override
    public void setData() {

    }

    @Override
    public void getContent(boolean isRefreshing) {

        if (isRefreshing) {
            swipeRefreshLayout.setRefreshing(false);
            List<Patient> patientList = new QueryDatabaseUtils().getPatients((sessionData != null ? sessionData.UserId : "n.a"));
            if (patientList != null) {
                if (patientList.size() > 0) {
                    patients = patientList;
                    recyclerView.setAdapter(new PatientCardAdapter(R.layout.card_row_patient_layout, getActivity(), systemProgressDialog, getActivity(),
                            patients, sessionData, toolbar, getFragmentManager(), searchLinearLayout,
                            toggleLayoutView, swipeLinearLayout, formDisplayLayout, toggleButton));
                }
            }
        } else {
            List<Patient> patientList = new QueryDatabaseUtils().getPatients((sessionData != null ? sessionData.UserId : "n.a"));
            if (patientList != null) {
                if (patientList.size() > 0) {
                    patients = patientList;
                    recyclerView.setAdapter(new PatientCardAdapter(R.layout.card_row_patient_layout, getActivity(), systemProgressDialog, getActivity(),
                            patients, sessionData, toolbar, getFragmentManager(),
                            searchLinearLayout, toggleLayoutView, swipeLinearLayout, formDisplayLayout, toggleButton));
                }
            }
        }
    }

    private void initialiseFilterForm(View view) {
        patientIdField = (EditText) view.findViewById(R.id.patientIdField);
        patientNameField = (EditText) view.findViewById(R.id.patientNameField);
        patientPhoneNumberField = (EditText) view.findViewById(R.id.patientPhoneNumberField);
        patientVillageNameField = (EditText) view.findViewById(R.id.patientVillageNameField);
        patientMeterNumberField = (EditText) view.findViewById(R.id.patientMeterNumberField);
        patientMethodOfDiagnosisField = (EditText) view.findViewById(R.id.patientMethodOfDiagnosisField);
        patientResultOfDiagnosisField = (EditText) view.findViewById(R.id.patientResultOfDiagnosisField);
        searchStartDate = (EditText) view.findViewById(R.id.patientDateOfBirthField);
        searchEndDate = (EditText) view.findViewById(R.id.patientDateOfDiagnosisField);

        patientGenderFemale = (RadioButton) view.findViewById(R.id.patientGenderFemale);
        patientGenderMale = (RadioButton) view.findViewById(R.id.patientGenderMale);
        patientGenderUnknown = (RadioButton) view.findViewById(R.id.patientGenderUnknown);

        patientGenderFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedGender = AppConstants.FEMALE;
            }
        });
        patientGenderMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedGender = AppConstants.MALE;
            }
        });
        patientGenderUnknown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedGender = AppConstants.UNKNOWN;
            }
        });


        patientNameField.setHint(AppConstants.NAME_HINT);
        patientIdField.setHint(AppConstants.PATIENT_ID_HINT);
        patientPhoneNumberField.setHint(AppConstants.PHONE_NUMBER_HINT);
        patientVillageNameField.setHint(AppConstants.VILLAGE_NAME_HINT);
        patientMeterNumberField.setHint(AppConstants.METER_NUMBER_HINT);
        patientMethodOfDiagnosisField.setHint(AppConstants.METHOD_OF_DIAGNOSIS_HINT);
        patientResultOfDiagnosisField.setHint(AppConstants.RESULT_OF_DIAGNOSIS_HINT);
        searchStartDate.setHint(AppConstants.DATE_HINT);
        searchEndDate.setHint(AppConstants.DATE_HINT);

        initializeStartAndEndDatesPickers(this);

        leftLinearLayout = (LinearLayout) view.findViewById(R.id.leftLinearLayout);
        rightLinearLayout = (LinearLayout) view.findViewById(R.id.rightLinearLayout);
        List<Disease> diseases = new QueryDatabaseUtils().getDiseases();
        int listSize = diseases.size();
        List<Disease> leftListItems = diseases.subList(0, listSize / 2);
        List<Disease> rightListItems = diseases.subList(listSize / 2, (listSize));
        for (Disease disease : leftListItems) {
            final CheckBox checkBox = new CheckBox(getActivity());
            setLayoutCheckBoxList(checkBox, disease);
            leftLinearLayout.addView(checkBox);
        }

        for (Disease disease : rightListItems) {
            final CheckBox checkBox = new CheckBox(getActivity());
            setLayoutCheckBoxList(checkBox, disease);
            rightLinearLayout.addView(checkBox);
        }

    }

    private void setLayoutCheckBoxList(CheckBox checkBox, Disease disease) {
        final CheckBoxItem checkBoxItem = new CheckBoxItem(disease.pk, disease.name);
        checkBox.setId(disease.pk);
        checkBox.setText(disease.name);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) selectedCheckBoxValue.add(checkBoxItem);
                else selectedCheckBoxValue.remove(checkBoxItem);
            }
        });
    }

    @Override
    public void queryAndFilteredData() {

    }

    private boolean isValidReportFilterDetails() {
        if (StringUtils.isBlank(patientNameField.getText())) {
            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Please select the date of diagnosis", getActivity());
            return false;
        } else if (StringUtils.isBlank(patientIdField.getText())) {
            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Please provide a patient ID", getActivity());
            return false;
        } else if (StringUtils.isBlank(searchStartDate.getText())) {
            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Please select the patient's date of birth", getActivity());
            return false;
        } else if (StringUtils.isBlank(patientMethodOfDiagnosisField.getText())) {
            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Please provide the method of diagnosis", getActivity());
            return false;
        } else if (StringUtils.isBlank(patientResultOfDiagnosisField.getText())) {
            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Please provide the result of diagnosis", getActivity());
            return false;
        } else if (StringUtils.isBlank(searchEndDate.getText())) {
            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Please select the date of diagnosis", getActivity());
            return false;
        } else {
            String dateOfBirth = new AppUtils().getJavaCurrentDate(new Date(String.valueOf(searchStartDate.getText())));
            String dateOfDiagnosis = new AppUtils().getJavaCurrentDate(new Date(String.valueOf(searchEndDate.getText())));
            long longDateOfBirth = AppUtils.javaDateFromString(dateOfBirth).getTime();
            long longDateOfDiagnosis = AppUtils.javaDateFromString(dateOfDiagnosis).getTime();
            if (longDateOfDiagnosis < longDateOfBirth) {
                AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "Date of diagnosis should not be defore date of birth", getActivity());
                return false;
            } else
                return true;
        }

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {

        }
    }

    @Override
    public void doSearch(String searchValue) {
        List<Patient> patientList = new QueryDatabaseUtils().getPatientBySearchValue((sessionData != null ? sessionData.UserId : "n.a"), searchValue);
        if (patientList != null) {
            if (patientList.size() > 0) {
                patients = patientList;
                recyclerView.setAdapter(new PatientCardAdapter(R.layout.card_row_patient_layout, getActivity(), systemProgressDialog, getActivity(),
                        patients, sessionData, toolbar, getFragmentManager(), searchLinearLayout,
                        toggleLayoutView, swipeLinearLayout, formDisplayLayout, toggleButton));
            }
        }
    }
}
