package com.sers.mobile.hims.models.transients;

public class CheckBoxItem {
    public int id;
    public String recordId;
    public String text;
    public String code;

    public CheckBoxItem() {
    }

    public CheckBoxItem(int id, String text) {
        this.id = id;
        this.text = text;
    }

    public CheckBoxItem(String code, int id, String text) {
        this.id = id;
        this.code = code;
        this.text = text;
    }

    public CheckBoxItem(int id, String recordId, String text) {
        this.id = id;
        this.recordId = recordId;
        this.text = text;
    }

    public CheckBoxItem(int id, String recordId, String text, String code) {
        this.id = id;
        this.recordId = recordId;
        this.text = text;
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object other) {

        return other != null && other instanceof CheckBoxItem && ((CheckBoxItem) other).text.equals(this.text);
    }

    @Override
    public int hashCode() {
        return this.text.hashCode() * 31;
    }
}
