package com.sers.mobile.hims.models.transients;


public class SessionData {
    public String IsLoggedIn;
    public String IsSessionAvailable;
    public String Id;
    public String EmailAddress;
    public String Username;
    public String FirstName;
    public String LastName;
    public String Gender;
    public String PhoneNumber;
    public String Password;
    public String ApiToken;
    public String UserId;
    public String Location;


    public SessionData(String isLoggedIn, String isSessionAvailable, String id, String emailAddress, String username,
                       String firstName, String lastName, String gender, String phoneNumber, String password,
                       String apiToken, String userId, String location) {
        IsLoggedIn = isLoggedIn;
        IsSessionAvailable = isSessionAvailable;
        Id = id;
        EmailAddress = emailAddress;
        Username = username;
        FirstName = firstName;
        LastName = lastName;
        Gender = gender;
        PhoneNumber = phoneNumber;
        Password = password;
        ApiToken = apiToken;
        UserId = userId;
        Location = location;
    }
}
