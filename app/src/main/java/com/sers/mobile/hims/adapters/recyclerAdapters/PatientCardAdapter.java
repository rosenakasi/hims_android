package com.sers.mobile.hims.adapters.recyclerAdapters;


import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.sers.mobile.hims.R;
import com.sers.mobile.hims.constants.AppConstants;
import com.sers.mobile.hims.constants.RecordStatus;
import com.sers.mobile.hims.custom.SystemProgressDialog;
import com.sers.mobile.hims.models.database.Patient;
import com.sers.mobile.hims.models.transients.SessionData;

import java.util.List;

public class PatientCardAdapter extends RecyclerView.Adapter<PatientCardAdapter.ViewHolder> {

    private List<Patient> listOfRecords = null;
    public Context context;
    public ColorGenerator generator = ColorGenerator.MATERIAL;
    public int rowLayout;
    public SystemProgressDialog systemProgressDialog;
    public Activity activity;
    private SessionData sessionData;
    private Toolbar toolbar = null;
    private FragmentManager supportFragmentManager = null;
    private LinearLayout messageLinearLayout, searchLinearLayout, toggleLayoutView, swipeLinearLayout;
    private RelativeLayout formDisplayLayout;
    private ToggleButton toggleButton;

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView thumbnail;
        TextView recordId, patientIdView, nameView, dateOfBirthView, genderView, phoneNumberView, nwscMeterNumberView,
                diseaseNameView, methodOfDiagnosisView, resultOfDiagnosisView, dateOfDiagnosisView, statusView, editRecordView, villageView;


        public ViewHolder(View view) {
            super(view);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            recordId = (TextView) view.findViewById(R.id.recordId);
            patientIdView = (TextView) view.findViewById(R.id.patientIdView);
            nameView = (TextView) view.findViewById(R.id.nameView);
            dateOfBirthView = (TextView) view.findViewById(R.id.dateOfBirthView);
            genderView = (TextView) view.findViewById(R.id.genderView);
            phoneNumberView = (TextView) view.findViewById(R.id.phoneNumberView);
            nwscMeterNumberView = (TextView) view.findViewById(R.id.nwscMeterNumberView);
            diseaseNameView = (TextView) view.findViewById(R.id.diseaseNameView);
            methodOfDiagnosisView = (TextView) view.findViewById(R.id.methodOfDiagnosisView);
            resultOfDiagnosisView = (TextView) view.findViewById(R.id.resultOfDiagnosisView);
            dateOfDiagnosisView = (TextView) view.findViewById(R.id.dateOfDiagnosisView);
            statusView = (TextView) view.findViewById(R.id.statusView);
            editRecordView = (TextView) view.findViewById(R.id.editRecordView);
            villageView = (TextView) view.findViewById(R.id.villageView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }


    public PatientCardAdapter(int rowLayout, Context context, SystemProgressDialog systemProgressDialog, Activity activity,
                              List<Patient> listOfRecords, SessionData sessionData, Toolbar toolbar,
                              FragmentManager supportFragmentManager, LinearLayout searchLinearLayout, LinearLayout toggleLayoutView, LinearLayout swipeLinearLayout,
                              RelativeLayout formDisplayLayout, ToggleButton toggleButton) {
        this.listOfRecords = listOfRecords;
        this.rowLayout = rowLayout;
        this.context = context;
        this.activity = activity;
        this.systemProgressDialog = systemProgressDialog;
        this.sessionData = sessionData;
        this.toolbar = toolbar;
        this.supportFragmentManager = supportFragmentManager;
        this.searchLinearLayout = searchLinearLayout;
        this.toggleLayoutView = toggleLayoutView;
        this.swipeLinearLayout = swipeLinearLayout;
        this.formDisplayLayout = formDisplayLayout;
        this.toggleButton = toggleButton;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (listOfRecords != null) {
            if (listOfRecords.size() == 0)
                holder.nameView.setText(AppConstants.NO_RECORDS);
            else {
                String recordId = listOfRecords.get(position).id;
                String status = listOfRecords.get(position).status;
                TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(listOfRecords.get(position).name.charAt(0)).toUpperCase(), generator.getRandomColor());
                holder.recordId.setText(recordId);
                holder.thumbnail.setImageDrawable(drawable);

                holder.patientIdView.setText(listOfRecords.get(position).patientId);
                holder.nameView.setText(listOfRecords.get(position).name);
                holder.dateOfBirthView.setText(listOfRecords.get(position).dateOfBirth);
                holder.genderView.setText(listOfRecords.get(position).gender);
                holder.phoneNumberView.setText(listOfRecords.get(position).phoneNumber);
                holder.nwscMeterNumberView.setText(listOfRecords.get(position).nwscMeterNumber);
                holder.diseaseNameView.setText(listOfRecords.get(position).diseaseName);
                holder.methodOfDiagnosisView.setText(listOfRecords.get(position).methodOfDiagnosis);
                holder.resultOfDiagnosisView.setText(listOfRecords.get(position).resultOfDiagnosis);
                holder.dateOfDiagnosisView.setText(listOfRecords.get(position).dateOfDiagnosis);
                holder.statusView.setText(listOfRecords.get(position).status);
                holder.villageView.setText(listOfRecords.get(position).villageName);

                holder.editRecordView.setText("Edit Patient");
                if (status != null) {
                    if (!status.equalsIgnoreCase(RecordStatus.SYNCED.getStatus())) {
                        holder.editRecordView.setVisibility(View.GONE);
                        holder.editRecordView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {


                            }
                        });
                    }
                }
            }
        } else holder.nameView.setText(AppConstants.NO_RECORDS);

    }


    @Override
    public int getItemCount() {
        return listOfRecords.size();
    }
}
