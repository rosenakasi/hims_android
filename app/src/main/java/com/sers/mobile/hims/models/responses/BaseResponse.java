package com.sers.mobile.hims.models.responses;


import java.io.Serializable;

public class BaseResponse implements Serializable {
    private String responseStatus;
    private String responseMessage;

    public BaseResponse() {
    }

    public BaseResponse(String responseStatus, String responseMessage) {
        this.responseStatus = responseStatus;
        this.responseMessage = responseMessage;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}
