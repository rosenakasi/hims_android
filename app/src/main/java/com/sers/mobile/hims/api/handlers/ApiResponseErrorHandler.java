package com.sers.mobile.hims.api.handlers;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

import retrofit.ErrorHandler;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ApiResponseErrorHandler implements ErrorHandler {
    Context context;
    Response r;

   public ApiResponseErrorHandler(Context context){
        this.context = context;
    }

    public ApiResponseErrorHandler(){

    }

    @Override
    public Throwable handleError(RetrofitError cause) {
         r = cause.getResponse();
        if (r != null) {
            Log.e("RETROFIT", r.getReason());
            Log.e("RETROFIT", String.valueOf(r.getStatus()));
            Log.e("RETROFIT", r.getUrl());
            switch (r.getStatus()) {
                case 400:
                    try {
                        Log.e("RETROFIT String", "" + convertStreamToString(r.getBody().in()));
                        return new BadRequestException(convertStreamToString(r.getBody().in()), cause);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case 401:
                    try {
                        Log.e("RETROFIT auth", "" + convertStreamToString(r.getBody().in()));
                        return new AuthFailedException(convertStreamToString(r.getBody().in()),cause);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case 404:

                    try {
                        return new ResourceNotFoundException(convertStreamToString(r.getBody().in()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
        return cause;
    }

    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}
