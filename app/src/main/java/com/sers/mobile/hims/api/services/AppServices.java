package com.sers.mobile.hims.api.services;


import com.sers.mobile.hims.models.requests.RequestSyncPatientInfo;
import com.sers.mobile.hims.models.requests.RequestUpdateProfile;
import com.sers.mobile.hims.models.requests.RequestUserLogin;
import com.sers.mobile.hims.models.responses.ResponseSyncedData;
import com.sers.mobile.hims.models.responses.ResponseUserLogin;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

public interface AppServices {

    @POST("/api/AppLoginService")
    void getAppLoginService(@Body RequestUserLogin requestUserLogin, Callback<ResponseUserLogin> cb);

    @POST("/api/AppUpdateProfileService")
    void updateProfileService(@Body RequestUpdateProfile requestUpdateProfile, Callback<ResponseUserLogin> cb);

    @POST("/api/SyncPatientInformationService")
    void syncPatientInformationService(@Body RequestSyncPatientInfo requestSyncPatientInfo, Callback<ResponseSyncedData> cb);
}
