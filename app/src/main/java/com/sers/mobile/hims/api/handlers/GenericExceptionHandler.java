package com.sers.mobile.hims.api.handlers;

import android.content.Context;
import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;


public class GenericExceptionHandler implements UncaughtExceptionHandler {

    private UncaughtExceptionHandler defaultUEH;
    Context context;
    public static final int WS_ERROR_LOG = 3;

    public GenericExceptionHandler(Context context) {
        this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        this.context = context;
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {

        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        e.printStackTrace(printWriter);
        String cause = "Cause: " + e.getCause();
        String clas = "Class: " + e.getClass();
        String localizedmsg = "LocalizedMessage: " + e.getLocalizedMessage();
        String msg = "Message: " + e.getMessage();
        String stacktraces = "StackTraces: " + e.getStackTrace();
        String stacktrace = result.toString();
        printWriter.close();

        String newDate = System.currentTimeMillis() + "";

        Log.v("TAG", "Date: " + newDate);

        Log.v("TAG", "StackTrace: " + stacktrace);
        Log.v("TAG", cause + "\n" + clas + "\n" + localizedmsg + "\n" + msg
                + "\n" + stacktraces + "\n");
        defaultUEH.uncaughtException(t, e);
    }
}
