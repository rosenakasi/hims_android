package com.sers.mobile.hims.constants;


public enum AppToolBarTitles {

    USER_PROFILE("Profile"),
    ABOUT_US("About Us"),
    HOME("Home"),
    SETTINGS("Settings"),
    PATIENTS("Patients"),
    LOGOUT("Logout");
    private String name;

    AppToolBarTitles(String name) {
        this.name = name;
    }

    /**
     * This method gets the name or title of the enumerator element
     *
     * @return the name
     */
    public String getName() {
        return name;
    }
}
