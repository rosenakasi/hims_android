package com.sers.mobile.hims.models.transients;


public class OfflineLogin {
    public String username;
    public String password;

    public OfflineLogin(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
