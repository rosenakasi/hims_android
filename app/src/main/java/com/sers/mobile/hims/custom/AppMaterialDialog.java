package com.sers.mobile.hims.custom;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDButton;
import com.sers.mobile.hims.R;

public class AppMaterialDialog {

    public MaterialDialog appMaterialDialog(int dialogLayout, Activity activity, String title,
                                            int neutralText, int negativeText, int positiveText, boolean cancelable) {
        final MaterialDialog dialog = new MaterialDialog.Builder(activity)
                .title(title)
                .customView(dialogLayout, Boolean.TRUE)
                .backgroundColorRes(R.color.white)
                .positiveColorRes(R.color.colorPrimary)
                .negativeColorRes(R.color.colorPrimary)
                .neutralColorRes(R.color.colorPrimary)
                .autoDismiss(false)
                .cancelable(cancelable)
                .neutralText(neutralText)
                .negativeText(negativeText)
                .positiveText(positiveText)
                .build();

        MDButton decline = (MDButton) dialog.getActionButton(DialogAction.NEGATIVE);
        decline.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        return dialog;
    }


    public Dialog ordinaryDialog(int dialogLayout, Activity activity, boolean cancelable) {
        Dialog dialog = new Dialog(activity, android.R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogLayout);
        dialog.setCanceledOnTouchOutside(cancelable);
        dialog.setCanceledOnTouchOutside(cancelable);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
        window.setSoftInputMode(layoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        window.setLayout(layoutParams.MATCH_PARENT, layoutParams.WRAP_CONTENT);
        return dialog;
    }
}
