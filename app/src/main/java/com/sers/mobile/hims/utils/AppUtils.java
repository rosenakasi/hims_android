package com.sers.mobile.hims.utils;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.sers.mobile.hims.sessionManager.SessionManager;
import com.sers.mobile.hims.constants.AppConstants;

import org.joda.time.Days;
import org.joda.time.LocalDate;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class AppUtils {

    public static final long CONNECTION_TIME_OUT = 300;
    public static int PERMITTED_DAYS_RANGE = 7;

	public static void main(String[] a) {
		String providedDate1 = "12/10/2017";
		String providedDate2 = "7/10/2017";
		String providedDate3 = "11/10/2017";
		// System.out.println(javaDateFromString(providedDate));
		String date1 = new AppUtils().getJavaCurrentDate(new Date(providedDate1));
		String date4 = new AppUtils().getJavaCurrentDate(new Date(providedDate3));

		long d1 = javaDateFromString(date1).getTime();
		long d4 = javaDateFromString(date4).getTime();

		System.out.println(d1);
		System.out.println(d4);

		if (d4 < d1) {
			System.out.println("Wrong date range");
		} else
			System.out.println("correct date range");

	}
	
    public static String getCurrentDate() {
        return new SimpleDateFormat("dd/MM/yyyy").format(new Date());
    }

	public String getJavaCurrentDate(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return dateFormat.format(date);
	}
	
	public static Date javaDateFromString(String providedDate) {
		String[] dateTokens = providedDate.split("/");
		Date processedDate = new GregorianCalendar(Integer.parseInt(dateTokens[2]), Integer.parseInt(dateTokens[1]),
				Integer.parseInt(dateTokens[0])).getTime();
		return processedDate;
	}
	
    public static void showSnackBarMessage(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

    public static void clearLoginSession(Activity activity) {
        new SessionManager(activity).clearSessionManager();
    }

    public static int getDaysBetweenDates(String startDate, String endDate) {
        return Days.daysBetween(new LocalDate(new Date(startDate != null ? startDate : AppUtils.getCurrentDate()).getTime()),
                new LocalDate(new Date(endDate != null ? endDate : AppUtils.getCurrentDate()).getTime())).getDays();
    }



    public List<String> getSpinnerItems(List<?> listOfModels) {
        List<String> list = new ArrayList<String>();
        try {
            if (listOfModels != null) {
                if (listOfModels.size() > 0) {
                    String[] spinnerItemNames = new String[listOfModels.size()];
                    for (int i = 0; i < listOfModels.size(); i++) {
                        spinnerItemNames[i] = listOfModels.get(i).toString();
                    }
                    for (int x = 0; x < spinnerItemNames.length; x++) {
                        list.add(spinnerItemNames[x]);
                    }
                    /*if (list != null)
                        Collections.sort(list);*/
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public void noInternet(final Context context, String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("No internet access. Please turn it on to continue with : " + message)
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, final int id) {
                        Intent i = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(i);

                    }

                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();

            }
        });
        AlertDialog alert = builder.create();
        alert.setCancelable(false);
        alert.show();
        Log.d("LOG_TAG", "No network available!");
    }

    public boolean checkNetwork(Context context, Activity activity) {
        if (isNetworkAvailable(activity)) {
            return true;
        } else {
            noInternet(context);
        }
        return false;
    }

    public void noInternet(final Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("No internet access. Please turn it on to use this Application")
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, final int id) {
                        Intent i = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(i);

                    }

                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();

            }
        });
        AlertDialog alert = builder.create();
        alert.setCancelable(false);
        alert.show();
        Log.d("LOG_TAG", "No network available!");
    }

    public boolean isNetworkAvailable(Activity activity) {
        boolean haveConnectedWifi = Boolean.FALSE;
        boolean haveConnectedMobile = Boolean.FALSE;
        boolean isNetworkConnected = Boolean.FALSE;

        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();

        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;

            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }

        System.out.println("***** Network type connected : " + isNetworkConnected);
        System.out.println("***** Network type wifi available : " + haveConnectedWifi);
        System.out.println("***** Network type data available : " + haveConnectedMobile);

        return haveConnectedWifi || haveConnectedMobile;
    }

    public static void showToastMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static String generateUserId() {
        return new BigInteger(130, new SecureRandom()).toString(32);
    }

    public static void showToastMessage(Context context, String message, int duration) {
        Toast.makeText(context, message, duration).show();
    }

    /**
     * @param message
     * @param activity
     */
    public static void showMessage(boolean cancelable, String title, String message, Activity activity) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int pressed) {
                switch (pressed) {
                    case DialogInterface.BUTTON_POSITIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(cancelable);
        builder.setTitle(title);
        builder.setMessage(message).setPositiveButton("Ok", dialogClickListener).show();
    }

    public static void showConfirmMessage(String message, Activity activity, DialogInterface.OnClickListener dialogClickListener) {
        DialogInterface.OnClickListener closeDialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int pressed) {
                switch (pressed) {
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setTitle(AppConstants.CONFIRM_OPERATION);
        builder.setMessage(message).setNegativeButton("No", closeDialogClickListener)
                .setPositiveButton("Yes", dialogClickListener)
                .show();
    }

    private static void showMessageWithOutButton(boolean cancelable, String title, String message, Activity activity) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int pressed) {
                switch (pressed) {
                    case DialogInterface.BUTTON_POSITIVE:
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(cancelable);
        builder.setTitle(title);
        builder.setMessage(message).show();
    }

}
