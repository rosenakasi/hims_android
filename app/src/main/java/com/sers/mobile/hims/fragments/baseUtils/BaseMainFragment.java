package com.sers.mobile.hims.fragments.baseUtils;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sers.mobile.hims.custom.SlidingTabLayout;
import com.sers.mobile.hims.utils.AppUtils;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BaseMainFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */

public abstract class BaseMainFragment extends Fragment implements View.OnClickListener {
    public SlidingTabLayout tabLayout;
    public android.support.v4.app.FragmentManager compactibleFragmentManager;
    public OnFragmentInteractionListener mListener;
    public ViewPager pager;
    public CharSequence titles[];
    public int tabCount;
    public String userId = "email@hims.com";
    public String startDate = AppUtils.getCurrentDate();
    public String endDate = AppUtils.getCurrentDate();
    public Toolbar toolbar = null;
    public Activity activity = null;
    public int previousPosition = 0;

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }


    public BaseMainFragment() {

    }

    public BaseMainFragment(android.support.v4.app.FragmentManager compactibleFragmentManager, CharSequence titles[], int tabCount) {
        this.compactibleFragmentManager = compactibleFragmentManager;
        this.titles = titles;
        this.tabCount = tabCount;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public abstract View onCreateView(LayoutInflater inflater, ViewGroup container,
                                      Bundle savedInstanceState);

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public static String makeFragmentName(int viewPagerId, int index) {
        return "android:switcher:" + viewPagerId + ":" + index;
    }

    public abstract boolean headerInformationIsValid();

    public abstract void setData();

}
