package com.sers.mobile.hims.fragments.navMenu;

import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.sers.mobile.hims.R;
import com.sers.mobile.hims.api.webserviceImpl.WebService;
import com.sers.mobile.hims.constants.AppConstants;
import com.sers.mobile.hims.constants.AppMessages;
import com.sers.mobile.hims.custom.SystemProgressDialog;
import com.sers.mobile.hims.databaseUtils.QueryDatabaseUtils;
import com.sers.mobile.hims.fragments.baseUtils.BaseMainFragment;
import com.sers.mobile.hims.models.requests.RequestUpdateProfile;
import com.sers.mobile.hims.models.transients.SessionData;
import com.sers.mobile.hims.utils.AppUtils;

import org.apache.commons.lang3.StringUtils;


public class ProfileFragment extends BaseMainFragment {

    private RadioButton profileEditPassword, profileDontEditPassword, profileGenderMale, profileGenderFemale, profileGenderUnknown;
    private String selectedGender = AppConstants.UNKNOWN;
    private boolean editPassword = true;
    private EditText profilePasswordField, profileConfirmPasswordField,
            profileFirstNameField, profileLastNameField, profilePhoneNumberField, profileUsernameField, profileEmailField;
    private TextView profilePasswordFieldHeader, profileConfirmPasswordFieldHeader;
    private SessionData sessionData;
    private SystemProgressDialog systemProgressDialog;
    private AppCompatButton accountButton;

    public ProfileFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_options_profile, container, false);
        profileFirstNameField = (EditText) rootView.findViewById(R.id.profileFirstNameField);
        profileLastNameField = (EditText) rootView.findViewById(R.id.profileLastNameField);
        profilePhoneNumberField = (EditText) rootView.findViewById(R.id.profilePhoneNumberField);
        profileEmailField = (EditText) rootView.findViewById(R.id.profileEmailField);
        profileUsernameField = (EditText) rootView.findViewById(R.id.profileUsernameField);

        profilePasswordFieldHeader = (TextView) rootView.findViewById(R.id.profilePasswordFieldHeader);
        profileConfirmPasswordFieldHeader = (TextView) rootView.findViewById(R.id.profileConfirmPasswordFieldHeader);
        profilePasswordField = (EditText) rootView.findViewById(R.id.profilePasswordField);
        profileConfirmPasswordField = (EditText) rootView.findViewById(R.id.profileConfirmPasswordField);
        profileEditPassword = (RadioButton) rootView.findViewById(R.id.profileEditPassword);
        profileDontEditPassword = (RadioButton) rootView.findViewById(R.id.profileDontEditPassword);
        profileGenderFemale = (RadioButton) rootView.findViewById(R.id.profileGenderFemale);
        profileGenderMale = (RadioButton) rootView.findViewById(R.id.profileGenderMale);
        profileGenderUnknown = (RadioButton) rootView.findViewById(R.id.profileGenderUnknown);

        systemProgressDialog = new SystemProgressDialog(getActivity());
        sessionData = QueryDatabaseUtils.getLoggedInUserInfo(getActivity());
        if (sessionData != null) {
            profileFirstNameField.setText(sessionData.FirstName);
            profileLastNameField.setText(sessionData.LastName);
            profilePhoneNumberField.setText(sessionData.PhoneNumber);
            profileUsernameField.setText(sessionData.Username);
            profileEmailField.setText(sessionData.EmailAddress);
            setUserGender(sessionData.Gender);
        }
        changeFieldStatus(View.VISIBLE);


        profileEditPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editPassword = true;
                changeFieldStatus(View.VISIBLE);
            }
        });
        profileDontEditPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editPassword = false;
                changeFieldStatus(View.GONE);
            }
        });

        profileGenderFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedGender = AppConstants.FEMALE;
            }
        });
        profileGenderMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedGender = AppConstants.MALE;
            }
        });
        profileGenderUnknown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedGender = AppConstants.UNKNOWN;
            }
        });

        accountButton = (AppCompatButton) rootView.findViewById(R.id.profileButton);
        accountButton.setOnClickListener(this);
        return rootView;
    }

    @Override
    public boolean headerInformationIsValid() {
        return false;
    }

    @Override
    public void setData() {

    }

    private void changeFieldStatus(int status) {
        profilePasswordFieldHeader.setVisibility(status);
        profileConfirmPasswordFieldHeader.setVisibility(status);
        profilePasswordField.setVisibility(status);
        profileConfirmPasswordField.setVisibility(status);
    }

    private void setUserGender(String gender) {
        if (gender != null) {
            if (gender.equalsIgnoreCase(AppConstants.MALE)) {
                if (profileGenderMale != null)
                    profileGenderMale.setChecked(true);
                if (profileGenderFemale != null)
                    profileGenderFemale.setChecked(false);
                if (profileGenderUnknown != null)
                    profileGenderUnknown.setChecked(false);
            } else if (gender.equalsIgnoreCase(AppConstants.FEMALE)) {
                if (profileGenderMale != null)
                    profileGenderMale.setChecked(false);
                if (profileGenderFemale != null)
                    profileGenderFemale.setChecked(true);
                if (profileGenderUnknown != null)
                    profileGenderUnknown.setChecked(false);
            } else {
                if (profileGenderMale != null)
                    profileGenderMale.setChecked(false);
                if (profileGenderFemale != null)
                    profileGenderFemale.setChecked(false);
                if (profileGenderUnknown != null)
                    profileGenderUnknown.setChecked(true);
            }
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.profileButton:
                updateUserProfile();
                break;
        }
    }

    private void updateUserProfile() {
        if (new AppUtils().isNetworkAvailable(getActivity())) {
            String firstName = String.valueOf(profileFirstNameField.getText());
            String lastName = String.valueOf(profileLastNameField.getText());
            String phoneNumber = String.valueOf(profilePhoneNumberField.getText());
            String username = String.valueOf(profileUsernameField.getText());
            String emailAddress = String.valueOf(profileEmailField.getText());
            String gender = selectedGender;
            String password = String.valueOf(profilePasswordField.getText());
            String confirmPassword = String.valueOf(profileConfirmPasswordField.getText());
            if (validateUserProfile(firstName, lastName, phoneNumber, "location", emailAddress, gender, password, confirmPassword, username)) {
                if (!editPassword) password = sessionData.Password;
                RequestUpdateProfile requestUpdateProfile = new RequestUpdateProfile(sessionData.UserId, firstName, lastName, gender,
                        username, password, phoneNumber, emailAddress);
                new WebService(systemProgressDialog, getActivity(), requestUpdateProfile).updateProfileService();
                profilePasswordField.setText("");
                profileConfirmPasswordField.setText("");
            }
        } else new AppUtils().noInternet(getActivity(), "Updating Your Account");
    }


    private boolean validateUserProfile(String firstName, String lastName, String phoneNumber, String location, String emailAddress,
                                        String gender, String password, String confirmPassword, String username) {
        if (StringUtils.isBlank(firstName)) {
            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, AppMessages.PROVIDE_FIRST_NAME, activity);
            return false;
        } else if (StringUtils.isBlank(lastName)) {
            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, AppMessages.PROVIDE_LAST_NAME, activity);
            return false;
        } else if (StringUtils.isBlank(phoneNumber)) {
            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, AppMessages.PROVIDE_PHONE_NUMBER, activity);
            return false;
        } else if (StringUtils.isBlank(location)) {
            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, AppMessages.PROVIDE_USER_LOCATION, activity);
            return false;
        } else if (StringUtils.isBlank(username)) {
            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, AppMessages.PROVIDE_USERNAME, activity);
            return false;
        } else if (StringUtils.isBlank(emailAddress)) {
            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, AppMessages.PROVIDE_EMAIL, activity);
            return false;
        } else if (StringUtils.isBlank(gender)) {
            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, AppMessages.PROVIDE_GENDER, activity);
            return false;
        } else {
            if (editPassword) {
                if (StringUtils.isBlank(password)) {
                    AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, AppMessages.PROVIDE_PASSWORD, activity);
                    return false;
                } else if (StringUtils.isBlank(confirmPassword)) {
                    AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, AppMessages.PROVIDE_PASSWORD_CONFIRM, activity);
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }
    }
}
