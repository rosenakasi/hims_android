package com.sers.mobile.hims.databaseUtils;


import android.content.Context;

import com.sers.mobile.hims.models.database.Disease;
import com.sers.mobile.hims.models.database.Patient;

import org.sers.kpa.datasource.datamanager.DatabaseAdapter;

import java.io.Serializable;

public class DatabaseCreationUtil implements Serializable {

    String DATABASE_NAME = "db_hims";
    private Context context;

    public DatabaseCreationUtil(Context context) {
        this.context = context;
        DatabaseAdapter.Util.createInstance(this.context, DATABASE_NAME,
                new Class<?>[]{Disease.class, Patient.class});
    }


}
