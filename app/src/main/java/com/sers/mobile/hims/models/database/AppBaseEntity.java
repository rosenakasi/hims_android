package com.sers.mobile.hims.models.database;


import org.sers.kpa.datasource.annotations.Column;
import org.sers.kpa.datasource.annotations.MappedSupperClass;
import org.sers.kpa.datasource.annotations.PK;

import java.io.Serializable;

@MappedSupperClass
public class AppBaseEntity implements Serializable {

    @PK
    @Column(name = "local_id", nullable = false)
    public int pk = 0;

    @Column(name = "id", nullable = true)
    public String id;

    @Column(name = "date", nullable = true)
    public String date;

    @Column(name = "user_id", nullable = true)
    public String userId;


    public AppBaseEntity() {
    }

    public AppBaseEntity(String id) {
        this.id = id;
    }

    public AppBaseEntity(String id, String date) {
        this.id = id;
        this.date = date;
    }

    public AppBaseEntity(int pk) {
        this.pk = pk;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
