package com.sers.mobile.hims.adapters.tabAdapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public abstract class BaseViewPagerAdapter extends FragmentStatePagerAdapter {
    public CharSequence titles[];
    public int tabCount;


    public BaseViewPagerAdapter(FragmentManager fm, CharSequence titles[], int tabCount) {
        super(fm);
        this.titles = titles;
        this.tabCount = tabCount;
    }

    /**
     * This method return the fragment for the every position in the View Pager
     */
    public abstract Fragment getItem(int position);

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return tabCount;
    }


}
