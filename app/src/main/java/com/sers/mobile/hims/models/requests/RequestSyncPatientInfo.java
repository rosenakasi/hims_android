package com.sers.mobile.hims.models.requests;

import java.io.Serializable;
import java.util.List;

public class RequestSyncPatientInfo implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userId;
	private String apiToken;
	private List<PatientInformation> patientInformations;

	public RequestSyncPatientInfo(String userId, String apiToken, List<PatientInformation> patientInformations) {
		this.userId = userId;
		this.apiToken = apiToken;
		this.patientInformations = patientInformations;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @return the apiToken
	 */
	public String getApiToken() {
		return apiToken;
	}

	/**
	 * @return the patientInformations
	 */
	public List<PatientInformation> getPatientInformations() {
		return patientInformations;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @param apiToken
	 *            the apiToken to set
	 */
	public void setApiToken(String apiToken) {
		this.apiToken = apiToken;
	}

	/**
	 * @param patientInformations
	 *            the patientInformations to set
	 */
	public void setPatientInformations(List<PatientInformation> patientInformations) {
		this.patientInformations = patientInformations;
	}

}
