package com.sers.mobile.hims.fragments.navMenu;

import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sers.mobile.hims.R;
import com.sers.mobile.hims.api.webserviceImpl.WebService;
import com.sers.mobile.hims.constants.AppMessages;
import com.sers.mobile.hims.constants.RecordStatus;
import com.sers.mobile.hims.custom.SystemProgressDialog;
import com.sers.mobile.hims.databaseUtils.QueryDatabaseUtils;
import com.sers.mobile.hims.fragments.baseUtils.BaseMainFragment;
import com.sers.mobile.hims.models.database.Patient;
import com.sers.mobile.hims.models.requests.PatientInformation;
import com.sers.mobile.hims.models.requests.RequestSyncPatientInfo;
import com.sers.mobile.hims.models.transients.SessionData;
import com.sers.mobile.hims.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;


public class SettingsFragment extends BaseMainFragment {

    private SessionData sessionData;
    private SystemProgressDialog systemProgressDialog;
    private AppCompatButton accountButton;
    private String apiToken = "n.a";

    public SettingsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        systemProgressDialog = new SystemProgressDialog(getActivity());
        sessionData = QueryDatabaseUtils.getLoggedInUserInfo(getActivity());
        if (sessionData != null) {
            userId = sessionData.UserId;
            apiToken = sessionData.ApiToken;
        }

        accountButton = (AppCompatButton) rootView.findViewById(R.id.syncPatientInfoButton);
        accountButton.setOnClickListener(this);
        return rootView;
    }

    @Override
    public boolean headerInformationIsValid() {
        return false;
    }

    @Override
    public void setData() {

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.syncPatientInfoButton:
                syncPatientInformation();
                break;
        }
    }

    private void syncPatientInformation() {
        try {
            if (new AppUtils().isNetworkAvailable(getActivity())) {
                List<Patient> unSyncedPatients = new QueryDatabaseUtils().getPatientsByUserIdAndStatus(userId, RecordStatus.UN_SYNCED.getStatus());
                if (unSyncedPatients != null) {
                    if (unSyncedPatients.size() > 0) {
                        List<PatientInformation> patientInformations = new ArrayList<PatientInformation>();

                        for (Patient patient : unSyncedPatients) {
                            if (patient != null) {
                                PatientInformation patientInformation = new PatientInformation();
                                patientInformation.setPatientId(patient.patientId);
                                patientInformation.setName(patient.name);
                                patientInformation.setDateOfBirth(patient.dateOfBirth);
                                patientInformation.setGender(patient.gender);
                                patientInformation.setPhoneNumber(patient.phoneNumber);
                                patientInformation.setNwscMeterNumber(patient.nwscMeterNumber);
                                patientInformation.setDiseaseId(patient.diseaseId);
                                patientInformation.setMethodOfDiagnosis(patient.methodOfDiagnosis);
                                patientInformation.setResultOfDiagnosis(patient.resultOfDiagnosis);
                                patientInformation.setDateOfDiagnosis(patient.dateOfDiagnosis);
                                patientInformation.setRecordId(patient.recordId);
                                patientInformation.setVillageName(patient.villageName);
                                patientInformations.add(patientInformation);
                            }
                        }

                        RequestSyncPatientInfo requestSyncPatientInfo = new RequestSyncPatientInfo(userId, apiToken, patientInformations);
                        new WebService(systemProgressDialog, getActivity(), requestSyncPatientInfo).syncPatientInformationService();

                    } else {
                        AppUtils.showMessage(true, AppMessages.OPERATION_SUCCESSFUL_MESSAGE_TITLE, "No patient information to Sync", getActivity());
                    }
                }
            } else {
                new AppUtils().noInternet(getActivity(), "patient information Sync");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
