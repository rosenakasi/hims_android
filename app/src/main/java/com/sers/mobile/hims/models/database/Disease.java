package com.sers.mobile.hims.models.database;

import org.sers.kpa.datasource.annotations.Column;
import org.sers.kpa.datasource.annotations.Entity;
import org.sers.kpa.datasource.annotations.Table;

@Entity
@Table(name = "diseases")
public class Disease extends AppBaseEntity {

    @Column(name = "name", nullable = true)
    public String name;

    public Disease() {
    }

    public Disease(String name) {
        this.name = name;
    }

    public Disease(String id, String name) {
        super(id);
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
