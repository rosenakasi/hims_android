package com.sers.mobile.hims.models.requests;


import java.io.Serializable;

public class RequestUpdateProfile implements Serializable {
    private String userId;
    private String firstName;
    private String lastName;
    private String location;
    private String gender;
    private String username;
    private String password;
    private String dateOfBirth;
    private String phoneNumber;
    private String emailAddress;
    private String isSessionAvailable;

    public RequestUpdateProfile() {
    }

    public RequestUpdateProfile(String userId, String firstName, String lastName, String gender,
                                String username, String password, String phoneNumber, String emailAddress) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.username = username;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getIsSessionAvailable() {
        return isSessionAvailable;
    }

    public void setIsSessionAvailable(String isSessionAvailable) {
        this.isSessionAvailable = isSessionAvailable;
    }
}
