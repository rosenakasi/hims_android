package com.sers.mobile.hims.sessionManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.sers.mobile.hims.models.responses.ResponseUserLogin;
import com.sers.mobile.hims.models.transients.SessionData;


public class SessionManager {
    // Shared Preferences
    public SharedPreferences pref;
    // Editor for Shared preferences
    Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Sharedpref file name
    private static final String PREF_NAME = "SessionPreferences";

    /**
     * make all Shared Preferences Keys variables public to access from outside
     */
    public static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_IS_LOGGED_IN = "isSessionAvailable";
    public static final String KEY_ID = "Id";
    public static final String KEY_USER_ID = "UserId";
    public static final String KEY_EMAIL_ADDRESS = "EmailAddress";
    public static final String KEY_USERNAME = "Username";
    public static final String KEY_FIRST_NAME = "FirstName";
    public static final String KEY_LAST_NAME = "LastName";
    public static final String KEY_GENDER = "Gender";
    public static final String KEY_PHONE_NUMBER = "PhoneNumber";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_COUNTRY_ID = "CountryId";
    public static final String KEY_API_TOKEN = "ApiToken";


    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();

    }

    /**
     * Create login session
     */

    public void createLoginSession(ResponseUserLogin responseUserProfile, String IsSessionAvailable, String password) {
        if (responseUserProfile != null) {
            editor.putBoolean(IS_LOGIN, true);
            editor.putString(KEY_IS_LOGGED_IN, IsSessionAvailable);
            editor.putString(KEY_ID, responseUserProfile.getId());
            editor.putString(KEY_USER_ID, responseUserProfile.getUserId());
            editor.putString(KEY_EMAIL_ADDRESS, responseUserProfile.getEmailAddress());
            editor.putString(KEY_USERNAME, responseUserProfile.getUsername());
            editor.putString(KEY_FIRST_NAME, responseUserProfile.getFirstName());
            editor.putString(KEY_LAST_NAME, responseUserProfile.getLastName());
            editor.putString(KEY_GENDER, responseUserProfile.getGender());
            editor.putString(KEY_PHONE_NUMBER, responseUserProfile.getPhoneNumber());
            editor.putString(KEY_API_TOKEN, responseUserProfile.getApiToken());
            editor.putString(KEY_PASSWORD, password);
            editor.commit();
        }
    }

    public void createLoginSession(ResponseUserLogin responseUserProfile, String IsSessionAvailable) {
        if (responseUserProfile != null) {
            editor.putBoolean(IS_LOGIN, true);
            editor.putString(KEY_IS_LOGGED_IN, IsSessionAvailable);
            editor.putString(KEY_ID, responseUserProfile.getId());
            editor.putString(KEY_USER_ID, responseUserProfile.getUserId());
            editor.putString(KEY_EMAIL_ADDRESS, responseUserProfile.getEmailAddress());
            editor.putString(KEY_USERNAME, responseUserProfile.getUsername());
            editor.putString(KEY_FIRST_NAME, responseUserProfile.getFirstName());
            editor.putString(KEY_LAST_NAME, responseUserProfile.getLastName());
            editor.putString(KEY_GENDER, responseUserProfile.getGender());
            editor.putString(KEY_PHONE_NUMBER, responseUserProfile.getPhoneNumber());
            editor.putString(KEY_API_TOKEN, responseUserProfile.getApiToken());
            editor.commit();
        }
    }


    /**
     * Check login method wil check user login status If false it will redirect
     * user to login page Else won't do anything
     */
    public boolean checkLogin() {
        // Check login status
        return this.isLoggedIn();
    }


    public SessionData getSessionData() {
        return new SessionData(
                pref.getString(KEY_IS_LOGGED_IN, "false"),
                pref.getString(KEY_IS_LOGGED_IN, "false"),
                pref.getString(KEY_ID, null),
                pref.getString(KEY_EMAIL_ADDRESS, null),
                pref.getString(KEY_USERNAME, null),
                pref.getString(KEY_FIRST_NAME, null),
                pref.getString(KEY_LAST_NAME, null),
                pref.getString(KEY_GENDER, null),
                pref.getString(KEY_PHONE_NUMBER, null),
                pref.getString(KEY_PASSWORD, null),
                pref.getString(KEY_API_TOKEN, null),
                pref.getString(KEY_USER_ID, null), "");
    }

    /**
     * Clear session details
     */
    public boolean clearSessionManager() {
        editor.clear();
        editor.commit();
        return true;

    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

}