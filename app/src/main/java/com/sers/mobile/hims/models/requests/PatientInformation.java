package com.sers.mobile.hims.models.requests;

import java.io.Serializable;

public class PatientInformation implements Serializable {

	/**
     *
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Patient info
	 */
	private String patientId;
	private String name;
	private String dateOfBirth;
	private String gender;
	private String phoneNumber;
	private String nwscMeterNumber;
	/**
	 * Clinical variables
	 */
	private String diseaseId;
	private String methodOfDiagnosis;
	private String resultOfDiagnosis;
	private String dateOfDiagnosis;
	private String recordId;
	private String villageName;

	public String getVillageName() {
		return villageName;
	}

	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}

	/**
	 * @return the patientId
	 */
	public String getPatientId() {
		return patientId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the dateOfBirth
	 */
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * @return the nwscMeterNumber
	 */
	public String getNwscMeterNumber() {
		return nwscMeterNumber;
	}
	/**
	 * @return the diseaseId
	 */
	public String getDiseaseId() {
		return diseaseId;
	}
	/**
	 * @return the methodOfDiagnosis
	 */
	public String getMethodOfDiagnosis() {
		return methodOfDiagnosis;
	}
	/**
	 * @return the resultOfDiagnosis
	 */
	public String getResultOfDiagnosis() {
		return resultOfDiagnosis;
	}
	/**
	 * @return the dateOfDiagnosis
	 */
	public String getDateOfDiagnosis() {
		return dateOfDiagnosis;
	}
	/**
	 * @return the recordId
	 */
	public String getRecordId() {
		return recordId;
	}
	/**
	 * @param patientId the patientId to set
	 */
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * @param nwscMeterNumber the nwscMeterNumber to set
	 */
	public void setNwscMeterNumber(String nwscMeterNumber) {
		this.nwscMeterNumber = nwscMeterNumber;
	}
	/**
	 * @param diseaseId the diseaseId to set
	 */
	public void setDiseaseId(String diseaseId) {
		this.diseaseId = diseaseId;
	}
	/**
	 * @param methodOfDiagnosis the methodOfDiagnosis to set
	 */
	public void setMethodOfDiagnosis(String methodOfDiagnosis) {
		this.methodOfDiagnosis = methodOfDiagnosis;
	}
	/**
	 * @param resultOfDiagnosis the resultOfDiagnosis to set
	 */
	public void setResultOfDiagnosis(String resultOfDiagnosis) {
		this.resultOfDiagnosis = resultOfDiagnosis;
	}
	/**
	 * @param dateOfDiagnosis the dateOfDiagnosis to set
	 */
	public void setDateOfDiagnosis(String dateOfDiagnosis) {
		this.dateOfDiagnosis = dateOfDiagnosis;
	}
	/**
	 * @param recordId the recordId to set
	 */
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

}
