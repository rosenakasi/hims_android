package com.sers.mobile.hims.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.sers.mobile.hims.R;
import com.sers.mobile.hims.constants.AppToolBarTitles;
import com.sers.mobile.hims.custom.NavigationController;
import com.sers.mobile.hims.databaseUtils.QueryDatabaseUtils;
import com.sers.mobile.hims.fragments.baseUtils.BaseMainFragment;
import com.sers.mobile.hims.fragments.navMenu.HomeFragment;
import com.sers.mobile.hims.fragments.navMenu.PatientsFragment;
import com.sers.mobile.hims.fragments.navMenu.SettingsFragment;
import com.sers.mobile.hims.fragments.navMenu.ProfileFragment;
import com.sers.mobile.hims.models.transients.SessionData;
import com.sers.mobile.hims.utils.AppUtils;
import com.sers.mobile.hims.utils.NavigationUtil;

import org.apache.commons.lang3.StringUtils;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, BaseMainFragment.OnFragmentInteractionListener {

    private Toolbar toolbar = null;
    private SessionData sessionData;
    private TextView businessUnitCode, name, email;
    public CharSequence titles[];
    public int tabCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(AppToolBarTitles.HOME.getName());
        setSupportActionBar(toolbar);
        sessionData = QueryDatabaseUtils.getLoggedInUserInfo(getApplicationContext());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        NavigationUtil.renderNavigationMenuItems(navigationView, sessionData);
        View header = navigationView.getHeaderView(0);
        businessUnitCode = (TextView) header.findViewById(R.id.businessUnitCode);
        name = (TextView) header.findViewById(R.id.userFullName);
        email = (TextView) header.findViewById(R.id.userEmail);

        if (sessionData != null) {
            if (!StringUtils.isBlank(sessionData.UserId))
                businessUnitCode.setText(sessionData.UserId);
            else businessUnitCode.setVisibility(View.GONE);
            if (!StringUtils.isBlank(sessionData.FirstName) || !StringUtils.isBlank(sessionData.LastName))
                name.setText(sessionData.FirstName + " " + sessionData.LastName);
            if (!StringUtils.isBlank(sessionData.EmailAddress))
                email.setText(sessionData.EmailAddress);
            else email.setVisibility(View.GONE);
        } else
            businessUnitCode.setVisibility(View.GONE);

        getSupportFragmentManager().beginTransaction().replace(R.id.flContent, new HomeFragment()).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        invalidateOptionsMenu();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Fragment fragment = null;

        if (id == R.id.nav_home) {
            toolbar.setTitle(AppToolBarTitles.HOME.getName());
            fragment = new HomeFragment();
        } else if (id == R.id.nav_patients) {
            toolbar.setTitle(AppToolBarTitles.PATIENTS.getName());
            fragment = new PatientsFragment(MainActivity.this, toolbar, getSupportFragmentManager());
        } else if (id == R.id.nav_profile) {
            toolbar.setTitle(AppToolBarTitles.USER_PROFILE.getName());
            fragment = new ProfileFragment();
        } else if (id == R.id.nav_settings) {
            toolbar.setTitle(AppToolBarTitles.SETTINGS.getName());
            fragment = new SettingsFragment();
        } else if (id == R.id.nav_logout) {
            //AppUtils.clearLoginSession(MainActivity.this);
            NavigationController.navigateToActivity(MainActivity.this, MainActivity.this, LoginActivity.class);
        }

        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.flContent, fragment).commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
