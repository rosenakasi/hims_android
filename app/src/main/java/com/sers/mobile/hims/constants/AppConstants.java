package com.sers.mobile.hims.constants;


import com.sers.mobile.hims.utils.AppUtils;

public interface AppConstants {
    String DOMAIN_CONTACT = "256703819876";
    String APPLICATION_DOMAIN = "Application Domain";
    String NO_RECORDS = "No records";
    String FEMALE = "Female";
    String MALE = "Male";
    String UNKNOWN = "Unknown";
    String CONFIRM_OPERATION = "Confirm Message";
    String DATE_HINT = "E.g " + AppUtils.getCurrentDate();
    String NAME_HINT = "E.g John Paul";
    String PATIENT_ID_HINT = "E.g JP00002";
    String PHONE_NUMBER_HINT = "E.g 0702000002";
    String VILLAGE_NAME_HINT = "E.g Kampala";
    String METER_NUMBER_HINT = "E.g 218892";
    String METHOD_OF_DIAGNOSIS_HINT = "E.g General body scan";
    String RESULT_OF_DIAGNOSIS_HINT = "E.g Doctor's result";

}
