package com.sers.mobile.hims.models.responses;


import java.io.Serializable;

public class SyncedDataRecord implements Serializable {
    private String recordId;
    private String status;
    private String patientId;
    private String name;
    private String dateOfBirth;
    private String gender;
    private String phoneNumber;
    private String nwscMeterNumber;
    private String diseaseId;
    private String methodOfDiagnosis;
    private String resultOfDiagnosis;
    private String dateOfDiagnosis;

    public SyncedDataRecord() {
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getNwscMeterNumber() {
        return nwscMeterNumber;
    }

    public void setNwscMeterNumber(String nwscMeterNumber) {
        this.nwscMeterNumber = nwscMeterNumber;
    }

    public String getDiseaseId() {
        return diseaseId;
    }

    public void setDiseaseId(String diseaseId) {
        this.diseaseId = diseaseId;
    }

    public String getMethodOfDiagnosis() {
        return methodOfDiagnosis;
    }

    public void setMethodOfDiagnosis(String methodOfDiagnosis) {
        this.methodOfDiagnosis = methodOfDiagnosis;
    }

    public String getResultOfDiagnosis() {
        return resultOfDiagnosis;
    }

    public void setResultOfDiagnosis(String resultOfDiagnosis) {
        this.resultOfDiagnosis = resultOfDiagnosis;
    }

    public String getDateOfDiagnosis() {
        return dateOfDiagnosis;
    }

    public void setDateOfDiagnosis(String dateOfDiagnosis) {
        this.dateOfDiagnosis = dateOfDiagnosis;
    }
}
