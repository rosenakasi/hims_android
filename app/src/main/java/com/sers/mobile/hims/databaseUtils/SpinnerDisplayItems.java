package com.sers.mobile.hims.databaseUtils;


import com.sers.mobile.hims.models.database.Disease;

import java.util.ArrayList;
import java.util.List;

public class SpinnerDisplayItems {

    public List<Disease> getDiseaseSpinnerItems(String defaultText) {
        List<Disease> list = new ArrayList<Disease>();
        List<Disease> dbList = new QueryDatabaseUtils().getDiseases();
        System.out.println("List size : " + dbList.size());
        if (dbList != null) {
            if (dbList.size() != 0) {
                list.add(new Disease(defaultText));
                for (Disease disease : dbList) {
                    list.add(disease);
                }
            } else list.add(new Disease(defaultText));
        } else list.add(new Disease(defaultText));
        return list;
    }

}
