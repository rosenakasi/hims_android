package com.sers.mobile.hims.api.services;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sers.mobile.hims.api.handlers.ApiResponseErrorHandler;
import com.sers.mobile.hims.sessionManager.DomainManager;
import com.sers.mobile.hims.utils.AppUtils;
import com.squareup.okhttp.OkHttpClient;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;


public class ApiClient {

    //private static final String DEFAULT_APPLICATION_DOMAIN = "http://192.168.0.101:7171/hims-gui";
   private static final String DEFAULT_APPLICATION_DOMAIN = "http://mcrops.jvmhost.net/hims";

    private static String getEndPoint(Context context) {
        System.out.println("*** Application domain api client : " + DEFAULT_APPLICATION_DOMAIN);
        return DEFAULT_APPLICATION_DOMAIN;
    }
   /* private static String getEndPoint(Context context) {
        DomainManager domainManager = new DomainManager(context);
        HashMap<String, String> domain = domainManager.getApplicationDomain();
        String applicationDomain = domain.get(DomainManager.KEY_APPLICATION_DOMAIN);
        System.out.println("*** Application domain api client : " + applicationDomain);
        return applicationDomain;
    }*/

    public static RestAdapter getRestAdapter(Context context) {
        return new RestAdapter.Builder().setEndpoint(getEndPoint(context))
                .setErrorHandler(new ApiResponseErrorHandler(context)).setLogLevel(RestAdapter.LogLevel.FULL).build();
    }

    public static RestAdapter getRestAdapter(Context context, ErrorHandler handler) {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(AppUtils.CONNECTION_TIME_OUT, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(AppUtils.CONNECTION_TIME_OUT, TimeUnit.SECONDS);
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();

        return new RestAdapter.Builder().setEndpoint(getEndPoint(context)).setErrorHandler(handler)
                .setClient(new OkClient(okHttpClient)).setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL).build();
    }

    public static AppServices buildAPIService(Context context) {
        RestAdapter restAdapter = getRestAdapter(context, new ApiResponseErrorHandler(context));
        return restAdapter.create(AppServices.class);
    }

}
