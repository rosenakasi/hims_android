package com.sers.mobile.hims.fragments.navMenu;


import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sers.mobile.hims.R;
import com.sers.mobile.hims.databaseUtils.QueryDatabaseUtils;
import com.sers.mobile.hims.fragments.baseUtils.BaseMainFragment;
import com.sers.mobile.hims.models.transients.SessionData;

import org.apache.commons.lang3.StringUtils;


public class HomeFragment extends BaseMainFragment {


    private SessionData sessionData;

    public HomeFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        sessionData = QueryDatabaseUtils.getLoggedInUserInfo(getActivity());
        TextView welcomeInfo = (TextView) rootView.findViewById(R.id.welcomeInfo);
        String name = sessionData.FirstName + " " + sessionData.LastName;
        if (!StringUtils.isBlank(name))
            welcomeInfo.setText(name + ",");

        return rootView;
    }

    @Override
    public boolean headerInformationIsValid() {
        return false;
    }

    @Override
    public void setData() {

    }

    @Override
    public void onClick(View view) {

    }
}
