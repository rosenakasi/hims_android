package com.sers.mobile.hims.models.responses;


import java.util.List;

public class ResponseSyncedData extends BaseResponse {

    private List<SyncedDataRecord> failedRecords;
    private List<SyncedDataRecord> successfulRecords;

    public ResponseSyncedData() {
    }

    public List<SyncedDataRecord> getFailedRecords() {
        return failedRecords;
    }

    public void setFailedRecords(List<SyncedDataRecord> failedRecords) {
        this.failedRecords = failedRecords;
    }

    public List<SyncedDataRecord> getSuccessfulRecords() {
        return successfulRecords;
    }

    public void setSuccessfulRecords(List<SyncedDataRecord> successfulRecords) {
        this.successfulRecords = successfulRecords;
    }
}
