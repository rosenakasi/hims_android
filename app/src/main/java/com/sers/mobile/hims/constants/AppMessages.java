package com.sers.mobile.hims.constants;


public interface AppMessages {
    String SUCCESS = "0";
    String FAILED = "-1";
    String ERROR_MESSAGE_TITLE = "Action Failed";
    String NO_LOGIN_RESPONSE = "No response from the server, please try again";
    String UNKNOWN_LOGIN_RESPONSE = "Unknown response from the server, please try again";
    String OPERATION_SUCCESSFUL_MESSAGE_TITLE = "Operation Successful";

    String PROVIDE_FIRST_NAME = "Please provide a first name";
    String PROVIDE_LAST_NAME = "Please provide a last name";
    String PROVIDE_USER_LOCATION = "Please provide a location";
    String PROVIDE_USERNAME = "Please provide a username";
    String PROVIDE_PHONE_NUMBER = "Please provide a phone number";
    String PROVIDE_EMAIL = "Please provide an email address";
    String PROVIDE_GENDER = "Please select a gender";
    String PROVIDE_PASSWORD = "Please provide a password";
    String PROVIDE_PASSWORD_CONFIRM = "Please confirm your password";

}
