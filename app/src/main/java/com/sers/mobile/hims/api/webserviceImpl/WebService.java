package com.sers.mobile.hims.api.webserviceImpl;


import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.melnykov.fab.FloatingActionButton;
import com.sers.mobile.hims.activities.MainActivity;
import com.sers.mobile.hims.api.services.ApiClient;
import com.sers.mobile.hims.api.services.AppServices;
import com.sers.mobile.hims.constants.AppMessages;
import com.sers.mobile.hims.constants.RecordStatus;
import com.sers.mobile.hims.custom.NavigationController;
import com.sers.mobile.hims.custom.SystemProgressDialog;
import com.sers.mobile.hims.databaseUtils.QueryDatabaseUtils;
import com.sers.mobile.hims.models.database.Disease;
import com.sers.mobile.hims.models.database.Patient;
import com.sers.mobile.hims.models.requests.BaseRequest;
import com.sers.mobile.hims.models.requests.RequestSyncPatientInfo;
import com.sers.mobile.hims.models.requests.RequestUpdateProfile;
import com.sers.mobile.hims.models.requests.RequestUserLogin;
import com.sers.mobile.hims.models.responses.ResponseDisease;
import com.sers.mobile.hims.models.responses.ResponseSyncedData;
import com.sers.mobile.hims.models.responses.ResponseUserLogin;
import com.sers.mobile.hims.models.responses.SyncedDataRecord;
import com.sers.mobile.hims.models.transients.OfflineLogin;
import com.sers.mobile.hims.models.transients.SessionData;
import com.sers.mobile.hims.sessionManager.OfflineLoginManager;
import com.sers.mobile.hims.sessionManager.SessionManager;
import com.sers.mobile.hims.utils.AppUtils;

import org.apache.commons.lang3.StringUtils;
import org.sers.kpa.datasource.datamanager.CustomPersistanceManager;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class WebService {

    private AppServices appServices = null;
    private SystemProgressDialog systemProgressDialog = null;
    private SwipeRefreshLayout swipeRefreshLayout = null;
    private RecyclerView recyclerView = null;
    private FragmentManager fragmentManager = null;
    private Activity activity = null;
    private Context context = null;
    private SessionData sessionData = null;
    private BaseRequest baseRequest = null;
    private LinearLayout searchLinearLayout, toggleLayoutView, swipeLinearLayout;
    private RelativeLayout formDisplayLayout;
    private Toolbar toolbar = null;
    private android.support.v4.app.FragmentManager supportFragmentManager = null;
    private Spinner spinner;
    private FloatingActionButton fab;
    private RequestSyncPatientInfo requestSyncPatientInfo;
    private RequestUpdateProfile requestUpdateProfile;
    private RequestUserLogin requestUserLogin;


    public WebService(SystemProgressDialog systemProgressDialog, Activity activity) {
        this.systemProgressDialog = systemProgressDialog;
        this.activity = activity;
        this.appServices = ApiClient.buildAPIService(this.activity.getApplicationContext());
    }

    public WebService(SystemProgressDialog systemProgressDialog, Activity activity, RequestUserLogin requestUserLogin) {
        this.activity = activity;
        this.appServices = ApiClient.buildAPIService(this.activity.getApplicationContext());
        this.systemProgressDialog = systemProgressDialog;
        this.context = activity;
        this.requestUserLogin = requestUserLogin;
    }

    public WebService(SystemProgressDialog systemProgressDialog, Activity activity, RequestUpdateProfile requestUpdateProfile) {
        this.activity = activity;
        this.appServices = ApiClient.buildAPIService(this.activity.getApplicationContext());
        this.systemProgressDialog = systemProgressDialog;
        this.context = activity;
        this.requestUpdateProfile = requestUpdateProfile;
    }

    public WebService(SystemProgressDialog systemProgressDialog, Activity activity, RequestSyncPatientInfo requestSyncPatientInfo) {
        this.activity = activity;
        this.appServices = ApiClient.buildAPIService(this.activity.getApplicationContext());
        this.systemProgressDialog = systemProgressDialog;
        this.context = activity;
        this.requestSyncPatientInfo = requestSyncPatientInfo;
    }

    public WebService(SystemProgressDialog systemProgressDialog, SwipeRefreshLayout swipeRefreshLayout,
                      RecyclerView recyclerView, FragmentManager fragmentManager, Activity activity,
                      SessionData sessionData, BaseRequest baseRequest,
                      LinearLayout searchLinearLayout, LinearLayout toggleLayoutView, LinearLayout swipeLinearLayout,
                      RelativeLayout formDisplayLayout, Toolbar toolbar, android.support.v4.app.FragmentManager supportFragmentManager,
                      Spinner spinner, FloatingActionButton fab) {
        this.activity = activity;
        this.appServices = ApiClient.buildAPIService(this.activity.getApplicationContext());
        this.systemProgressDialog = systemProgressDialog;
        this.swipeRefreshLayout = swipeRefreshLayout;
        this.recyclerView = recyclerView;
        this.fragmentManager = fragmentManager;
        this.context = activity;
        this.sessionData = sessionData;
        this.baseRequest = baseRequest;
        this.searchLinearLayout = searchLinearLayout;
        this.toggleLayoutView = toggleLayoutView;
        this.swipeLinearLayout = swipeLinearLayout;
        this.formDisplayLayout = formDisplayLayout;
        this.toolbar = toolbar;
        this.supportFragmentManager = supportFragmentManager;
        this.spinner = spinner;
        this.fab = fab;
    }


    /**
     * Login initializers
     */
    public void getAppLoginService() {
        systemProgressDialog.showProgressDialog("Authenticating user details, Please Wait....");
        appServices.getAppLoginService(requestUserLogin, new Callback<ResponseUserLogin>() {
            @Override
            public void success(ResponseUserLogin responseUserLogin, Response response) {
                System.out.println("responseUserLogin : " + responseUserLogin);
                if (responseUserLogin != null) {
                    if (responseUserLogin.getResponseStatus().equalsIgnoreCase(AppMessages.SUCCESS)) {
                        SessionManager sessionManager = new SessionManager(activity);
                        sessionManager.clearSessionManager();
                        sessionManager.createLoginSession(responseUserLogin, String.valueOf(Boolean.TRUE), requestUserLogin.getPassword());

                        OfflineLoginManager offlineLoginManager = new OfflineLoginManager(activity);
                        offlineLoginManager.clearSessionManager();
                        offlineLoginManager.createLoginSession(requestUserLogin.getUserId(), requestUserLogin.getPassword());

                        List<ResponseDisease> responseDiseases = responseUserLogin.getDiseases();
                        if (responseDiseases != null) {
                            if (responseDiseases.size() > 0) {
                                try {
                                    new CustomPersistanceManager<Disease>(Disease.class).emptyTable();
                                    for (ResponseDisease responseDisease : responseDiseases) {
                                        new CustomPersistanceManager<Disease>(Disease.class).saveOrUpdateAndReturnModel(
                                                new Disease(responseDisease.getId(), responseDisease.getName()));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        NavigationController.navigateToActivity(activity, activity.getApplicationContext(), MainActivity.class);
                        AppUtils.showToastMessage(activity.getApplicationContext(), "Successful Login");
                        systemProgressDialog.closeProgressDialog();
                    } else if (responseUserLogin.getResponseStatus().equalsIgnoreCase(AppMessages.FAILED)) {
                        systemProgressDialog.closeProgressDialog();
                        AppUtils.showMessage(Boolean.TRUE, AppMessages.ERROR_MESSAGE_TITLE, responseUserLogin.getResponseMessage(), activity);
                    } else {
                        systemProgressDialog.closeProgressDialog();
                        AppUtils.showMessage(Boolean.TRUE, AppMessages.ERROR_MESSAGE_TITLE, AppMessages.UNKNOWN_LOGIN_RESPONSE, activity);
                    }
                } else {
                    systemProgressDialog.closeProgressDialog();
                    AppUtils.showMessage(Boolean.TRUE, AppMessages.ERROR_MESSAGE_TITLE, AppMessages.NO_LOGIN_RESPONSE, activity);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                systemProgressDialog.closeProgressDialog();
                error.printStackTrace();
                AppUtils.showMessage(Boolean.TRUE, AppMessages.ERROR_MESSAGE_TITLE, error.getMessage() != null ? error.getMessage() : AppMessages.NO_LOGIN_RESPONSE, activity);
            }
        });
    }

    public void updateProfileService() {
        systemProgressDialog.showProgressDialog("Updating user profile, Please Wait....");
        appServices.updateProfileService(requestUpdateProfile, new Callback<ResponseUserLogin>() {
            @Override
            public void success(ResponseUserLogin responseUserLogin, Response response) {
                System.out.println("responseUserLogin : " + responseUserLogin);
                if (responseUserLogin != null) {
                    if (responseUserLogin.getResponseStatus().equalsIgnoreCase(AppMessages.SUCCESS)) {
                        SessionManager sessionManager = new SessionManager(activity);
                        sessionManager.clearSessionManager();
                        sessionManager.createLoginSession(responseUserLogin, String.valueOf(Boolean.TRUE));
                        AppUtils.showToastMessage(activity.getApplicationContext(), "Successfully updated user profile");
                        systemProgressDialog.closeProgressDialog();
                    } else if (responseUserLogin.getResponseStatus().equalsIgnoreCase(AppMessages.FAILED)) {
                        systemProgressDialog.closeProgressDialog();
                        AppUtils.showMessage(Boolean.TRUE, AppMessages.ERROR_MESSAGE_TITLE, responseUserLogin.getResponseMessage(), activity);
                    } else {
                        systemProgressDialog.closeProgressDialog();
                        AppUtils.showMessage(Boolean.TRUE, AppMessages.ERROR_MESSAGE_TITLE, AppMessages.UNKNOWN_LOGIN_RESPONSE, activity);
                    }
                } else {
                    systemProgressDialog.closeProgressDialog();
                    AppUtils.showMessage(Boolean.TRUE, AppMessages.ERROR_MESSAGE_TITLE, AppMessages.NO_LOGIN_RESPONSE, activity);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                systemProgressDialog.closeProgressDialog();
                error.printStackTrace();
                AppUtils.showMessage(Boolean.TRUE, AppMessages.ERROR_MESSAGE_TITLE, error.getMessage() != null ? error.getMessage() : AppMessages.NO_LOGIN_RESPONSE, activity);
            }
        });
    }

    public void syncPatientInformationService() {
        systemProgressDialog.showProgressDialog("Synchronizing Patient information, Please Wait....");
        appServices.syncPatientInformationService(requestSyncPatientInfo, new Callback<ResponseSyncedData>() {
            @Override
            public void success(ResponseSyncedData responseSyncedData, Response response) {

                if (responseSyncedData != null) {
                    if (responseSyncedData.getResponseStatus().equalsIgnoreCase(AppMessages.SUCCESS)) {
                        systemProgressDialog.closeProgressDialog();

                        List<SyncedDataRecord> failedRecords = responseSyncedData.getFailedRecords();
                        List<SyncedDataRecord> successfulRecords = responseSyncedData.getSuccessfulRecords();

                        int failedNumbers = 0;
                        int successfulNumbers = 0;
                        if (failedRecords != null) failedNumbers = failedRecords.size();

                        if (successfulRecords != null) {
                            successfulNumbers = successfulRecords.size();
                            for (SyncedDataRecord syncedDataRecord : successfulRecords) {
                                Patient patient = new QueryDatabaseUtils().getPatientByRecordId(syncedDataRecord.getRecordId());
                                if (patient != null) {
                                    System.out.println("Not Null patient info");
                                    patient.status = RecordStatus.SYNCED.getStatus();
                                    try {
                                        new CustomPersistanceManager<Patient>(Patient.class).saveOrUpdateAndReturnModel(patient);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    System.out.println("Null : patient info");
                                    systemProgressDialog.closeProgressDialog();
                                }
                            }
                        }
                        String message = "Successfully synced : " + successfulNumbers + " records \nFailed to Sync : " + failedNumbers + " records";
                        if (successfulNumbers == 0 && failedNumbers == 0) {
                            message = "Application recognized by the server, Please click on the sync button again for your data to be synced";
                        }
                        AppUtils.showMessage(Boolean.TRUE, AppMessages.OPERATION_SUCCESSFUL_MESSAGE_TITLE,
                                message, activity);

                    } else if (responseSyncedData.getResponseStatus().equalsIgnoreCase(AppMessages.FAILED)) {
                        systemProgressDialog.closeProgressDialog();
                        AppUtils.showMessage(Boolean.TRUE, AppMessages.ERROR_MESSAGE_TITLE, responseSyncedData.getResponseMessage(), activity);
                    } else {
                        systemProgressDialog.closeProgressDialog();
                        AppUtils.showMessage(Boolean.TRUE, AppMessages.ERROR_MESSAGE_TITLE, AppMessages.UNKNOWN_LOGIN_RESPONSE, activity);
                    }
                } else {
                    systemProgressDialog.closeProgressDialog();
                    AppUtils.showMessage(Boolean.TRUE, AppMessages.ERROR_MESSAGE_TITLE, AppMessages.NO_LOGIN_RESPONSE, activity);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                systemProgressDialog.closeProgressDialog();
                error.printStackTrace();
                AppUtils.showMessage(Boolean.TRUE, AppMessages.ERROR_MESSAGE_TITLE, error.getMessage() != null ? error.getMessage() : AppMessages.NO_LOGIN_RESPONSE, activity);
            }
        });
    }

}
