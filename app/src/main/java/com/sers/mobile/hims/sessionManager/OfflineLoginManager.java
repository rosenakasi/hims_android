package com.sers.mobile.hims.sessionManager;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.sers.mobile.hims.models.responses.ResponseUserLogin;
import com.sers.mobile.hims.models.transients.OfflineLogin;
import com.sers.mobile.hims.models.transients.SessionData;


public class OfflineLoginManager {
    // Shared Preferences
    public SharedPreferences pref;
    // Editor for Shared preferences
    Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Sharedpref file name
    private static final String PREF_NAME = "OfflineLoginManager";

    /**
     * make all Shared Preferences Keys variables public to access from outside
     */
    public static final String KEY_USERNAME = "username";
    public static final String KEY_PASSWORD = "password";


    // Constructor
    public OfflineLoginManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();

    }

    /**
     * Create login session
     */

    public void createLoginSession(String username, String password) {
        if (username != null && password != null) {
            editor.putString(KEY_USERNAME, username);
            editor.putString(KEY_PASSWORD, password);
            editor.commit();
        }
    }


    public OfflineLogin getOfflineLoginInformation() {
        return new OfflineLogin(
                pref.getString(KEY_USERNAME, null),
                pref.getString(KEY_PASSWORD, null));
    }

    /**
     * Clear session details
     */
    public boolean clearSessionManager() {
        editor.clear();
        editor.commit();
        return true;

    }


}