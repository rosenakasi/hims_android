package com.sers.mobile.hims.models.database;


import org.sers.kpa.datasource.annotations.Column;
import org.sers.kpa.datasource.annotations.Entity;
import org.sers.kpa.datasource.annotations.Table;

@Entity
@Table(name = "patients")
public class Patient extends AppBaseEntity {
    /**
     * Patient info
     */
    @Column(name = "patient_id", nullable = true)
    public String patientId;

    @Column(name = "name", nullable = true)
    public String name;

    @Column(name = "date_of_birth", nullable = true)
    public String dateOfBirth;

    @Column(name = "gender", nullable = true)
    public String gender;

    @Column(name = "phone_number", nullable = true)
    public String phoneNumber;

    @Column(name = "nwsc_meter_number", nullable = true)
    public String nwscMeterNumber;
    /**
     * Clinical variables
     */
    @Column(name = "disease_id", nullable = true)
    public String diseaseId;

    @Column(name = "disease_name", nullable = true)
    public String diseaseName;

    @Column(name = "method_of_diagnosis", nullable = true)
    public String methodOfDiagnosis;

    @Column(name = "result_of_diagnosis", nullable = true)
    public String resultOfDiagnosis;

    @Column(name = "date_of_diagnosis", nullable = true)
    public String dateOfDiagnosis;

    @Column(name = "record_id", nullable = true)
    public String recordId;

    @Column(name = "status", nullable = true)
    public String status;

    @Column(name = "village_name", nullable = true)
    public String villageName;

    public Patient() {
    }


    public Patient(String id, String date, String patientId, String name, String dateOfBirth, String gender, String phoneNumber,
                   String nwscMeterNumber, String diseaseId, String diseaseName, String methodOfDiagnosis, String resultOfDiagnosis,
                   String dateOfDiagnosis, String recordId, String status, String villageName) {
        super(id, date);
        this.patientId = patientId;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.phoneNumber = phoneNumber;
        this.nwscMeterNumber = nwscMeterNumber;
        this.diseaseId = diseaseId;
        this.diseaseName = diseaseName;
        this.methodOfDiagnosis = methodOfDiagnosis;
        this.resultOfDiagnosis = resultOfDiagnosis;
        this.dateOfDiagnosis = dateOfDiagnosis;
        this.recordId = recordId;
        this.status = status;
        this.villageName = villageName;
    }
}
