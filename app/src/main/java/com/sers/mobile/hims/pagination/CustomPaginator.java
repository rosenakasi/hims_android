package com.sers.mobile.hims.pagination;

import android.app.Activity;

import com.sers.mobile.hims.constants.AppMessages;
import com.sers.mobile.hims.fragments.baseUtils.BaseMainFragment;
import com.sers.mobile.hims.utils.AppUtils;


public abstract class CustomPaginator extends BaseMainFragment {

    protected int nextPage = 1;
    protected int previousPage = -1;
    protected int totalDatabaseRecordsCount;
    protected int offset = 0;
    public static final int PAGE_SIZE = 10;
    public Activity activity;

    public CustomPaginator() {
    }

    public CustomPaginator(Activity activity) {
        this.activity = activity;
    }

    // public abstract Object[] getRowData(T model, int rowCounter);

    public abstract void queryAndFilteredData();

    public void loadNext() {
        if (hasMoreDatabaseRecords()) {
            offset = (nextPage * PAGE_SIZE);
            queryAndFilteredData();
            nextPage++;
            previousPage++;
        } else {
            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "No more records to show", activity);
        }
    }

    public void loadPrevious() {
        if (canLoadPrevious()) {
            offset = (previousPage * PAGE_SIZE);
            queryAndFilteredData();
            previousPage--;
            nextPage--;
        } else {
            AppUtils.showMessage(true, AppMessages.ERROR_MESSAGE_TITLE, "No previous records to show", activity);
        }
    }

    private boolean hasMoreDatabaseRecords() {
        return PAGE_SIZE * nextPage < this.totalDatabaseRecordsCount;
    }

    public boolean canLoadPrevious() {
        return this.previousPage >= 0;
    }

    /*public void setData(List<T> models) {
        //super.emptyTableModel();
        //super.setObjectsInModel(models);
        int rowCounter = offset + 1;
        for (T model : models) {
            //super.addRow(getRowData(model, rowCounter));
            rowCounter++;
        }
    }*/
}