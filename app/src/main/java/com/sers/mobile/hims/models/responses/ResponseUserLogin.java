package com.sers.mobile.hims.models.responses;


import java.util.List;

public class ResponseUserLogin extends BaseResponse {
    private String apiToken;
    private String Id;
    private String userId;
    private String username;
    private String emailAddress;
    private String firstName;
    private String lastName;
    private String location;
    private String gender;
    private String phoneNumber;
    private List<ResponseDisease> diseases;

    public ResponseUserLogin() {
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<ResponseDisease> getDiseases() {
        return diseases;
    }

    public void setDiseases(List<ResponseDisease> diseases) {
        this.diseases = diseases;
    }
}
