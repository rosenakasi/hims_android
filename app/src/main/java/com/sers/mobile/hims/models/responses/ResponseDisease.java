package com.sers.mobile.hims.models.responses;


import java.io.Serializable;

public class ResponseDisease implements Serializable {
    private String id;
    private String name;

    public ResponseDisease() {
    }

    public ResponseDisease(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
