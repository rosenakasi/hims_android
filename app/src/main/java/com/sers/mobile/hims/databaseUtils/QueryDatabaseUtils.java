package com.sers.mobile.hims.databaseUtils;


import android.content.Context;

import com.sers.mobile.hims.models.database.Disease;
import com.sers.mobile.hims.models.database.Patient;
import com.sers.mobile.hims.models.transients.SessionData;
import com.sers.mobile.hims.sessionManager.SessionManager;

import org.sers.kpa.datasource.datamanager.CustomDataAccessManager;

import java.util.List;

public class QueryDatabaseUtils {

    public static SessionData getLoggedInUserInfo(Context context) {
        return new SessionManager(context).getSessionData();
    }


    public List<Patient> getPatients(String userId) {
        try {
            return new CustomDataAccessManager(Patient.class)
                    .addFilterEqualTo("userId", userId)
                    .sortDesc("date")
                    .searchDatabase();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Patient> getPatientBySearchValue(String userId, String searchValue) {
        try {
            return new CustomDataAccessManager(Patient.class)
                    .addFilterEqualTo("userId", userId)
                    .addFilterLike("name", searchValue)
                    .sortDesc("date")
                    .searchDatabase();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Patient> getPatientsByUserIdAndStatus(String userId, String status) {
        try {
            return new CustomDataAccessManager(Patient.class)
                    .addFilterEqualTo("userId", userId)
                    .addFilterLike("status", status)
                    .searchDatabase();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Patient getPatientByRecordId(String recordId) {
        try {
            return (Patient) new CustomDataAccessManager(Patient.class)
                    .addFilterEqualTo("recordId", recordId).searchUnique();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Disease> getDiseases() {
        try {
            return new CustomDataAccessManager(Disease.class)
                    .searchDatabase();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Disease getDiseaseByName(String name) {
        try {
            return (Disease) new CustomDataAccessManager(Disease.class)
                    .addFilterEqualTo("name", name).searchUnique();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Disease getDiseaseByPrimaryKey(int pk) {
        try {
            return (Disease) new CustomDataAccessManager(Disease.class)
                    .addFilterEqualTo("pk", pk).searchUnique();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
